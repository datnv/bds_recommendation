import sys
sys.path.append("../")
import cassandra
from cassandra.cluster import Cluster
import json

cluster = Cluster(['172.16.0.11'], port=9042)

session = cluster.connect('dvglog')

with open('file txt/data.txt', 'w', encoding='utf-8') as f1:
    previous_date = None
    for date in range(12,19):
        for hour in range(0, 24):
            rows = session.execute(
                'select current_uri,user_session_id,date,time,data from dvglog.tracking where web_id=1 and year=2018 and month = 12 and date =' + str(
                    date) + ' and hour=' + str(hour) + ' and action_id=185 ALLOW FILTERING;')
            dic_data = {}

            for user_row in rows:
                current_uri = user_row.current_uri
                # if 'https://batdongsan.com.vn' in current_uri and ('-pr' in current_uri or 'd+' in current_uri):
                if 'https://batdongsan.com.vn' in current_uri:
                    user_session_id = user_row.user_session_id
                    date = user_row.date
                    time = user_row.time
                    if date != previous_date:
                        print(date)
                        previous_date = date
                    #print(user_row.hour)
                    data = user_row.data
                    data_json = json.loads(data)
                    if data_json['cc'] != None:
                        try:
                            cc = data_json['cc'] if data_json['cc'] != "0" and data_json['cc']!= ""  else None
                            ci = data_json['ci'] if data_json['ci'] != 0 and data_json['ci']!= "" else None
                            di = data_json['di'] if data_json['di'] != "0" and data_json['di']!= "" else None
                            pi = data_json['pi'] if data_json['pi'] != "0" and data_json['pi']!= "" else None
                            si = data_json['si'] if data_json['si'] != "0" and data_json['si']!= "" else None
                            wi = data_json['wi'] if data_json['wi'] != "0" and data_json['wi']!= "" else None

                            # find p,a
                            vdi = data_json['vdi']
                            p = vdi['p'] if 'p' in vdi else None
                            a = vdi['a'] if 'a' in vdi else None
                            f1.write(
                                str(user_session_id)+ '\t' + str(time) + '\t' + str(cc) + '\t' + str(ci) + '\t' + str(di) + '\t' + str(
                                    pi) + '\t' + str(si) + '\t' + str(wi) + '\t' + str(p) + '\t' + str(a) + '\n')
                        except Exception as e:
                            print(e)
                            print(data)
