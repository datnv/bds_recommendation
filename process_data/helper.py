import sys
sys.path.append("../")
import numpy as np


def save_dic(dic, path):
    #print('save dic to ' + path)
    list = []
    for key in dic.keys():
        list.append([key, dic[key]])
    # list = np.array(list)
    np.save(path, np.array(list, dtype=object))
    #print(type(list[0][0]))
    #print(type(list[0][1]))
    #print('done')


def load_dic(path):
    #print('load dic in ' + path)
    list = np.load(path)
    #print(type(list[0][0]))
    #print(type(list[0][1]))
    dic = {}
    for i in range(len(list)):
        dic[list[i][0]] = list[i][1]
    return dic


def convert_pa_string_to_id(p_string, a_string):
    a = None
    p = None
    type_p = None
    a_id = None
    p_id = None
    a_string = a_string.lower().strip().replace(',', '')
    p_string = p_string.lower().strip().replace(',', '')
    p_string = p_string.replace('trên', '')
    p_string = p_string.replace('dưới', '')
    a_string = a_string.replace('trên', '')
    a_string = a_string.replace('dưới', '')
    if '-' in p_string:
        index = p_string.find('-')
        p_string = p_string[:index:]
    if '-' in a_string:
        index = a_string.find('-')
        a_string = a_string[:index:]
    if 'm²' in a_string or 'm2' in a_string:
        a_string = a_string.replace('m²', ' ')
        a_string = a_string.replace('m2', ' ')

        a = float(a_string)
    elif a_string != 'none' and a_string != 'không xác định':
        print('a not valid: ' + str(a_string))

    if 'tháng' in p_string:
        type_p = 0
        if not ('m²' in p_string or 'm2' in p_string):
            if 'triệu/tháng' in p_string:
                p_string = p_string.replace('triệu/tháng', ' ')
                p = float(p_string) * 1000000
            elif 'nghìn/tháng' in p_string:
                p_string = p_string.replace('nghìn/tháng', ' ')
                p = float(p_string) * 1000
            elif 'tỷ/tháng' in p_string:
                p_string = p_string.replace('tỷ/tháng', ' ')
                p = float(p_string) * 1000000000
            elif p_string != 'none':
                print('p 1 not valid: ' + str(p_string))
        else:
            if ('nghìn/m2/tháng' in p_string or 'nghìn/m²/tháng' in p_string) and a != None:
                p_string = p_string.replace('nghìn/m2/tháng', ' ')
                p_string = p_string.replace('nghìn/m²/tháng', ' ')
                p = float(p_string) * 1000 * a
            elif ('triệu/m2/tháng' in p_string or 'triệu/m²/tháng' in p_string) and a != None:
                p_string = p_string.replace('triệu/m2/tháng', ' ')
                p_string = p_string.replace('triệu/m²/tháng', ' ')
                p = float(p_string) * 1000000 * a
            elif ('tỷ/m2/tháng' in p_string or 'tỷ/m²/tháng' in p_string) and a != None:
                p_string = p_string.replace('tỷ/m2/tháng', ' ')
                p_string = p_string.replace('tỷ/m²/tháng', ' ')
                p = float(p_string) * 1000000000 * a
            elif p_string != 'none' and a!=None:
                print('p 2 not valid: ' + str(p_string))
    else:
        type_p = 1
        if not ('m²' in p_string or 'm2' in p_string):
            if 'triệu' in p_string:
                p_string = p_string.replace('triệu', ' ')
                p = float(p_string) * 1000000
            elif 'tỷ' in p_string:
                p_string = p_string.replace('tỷ', ' ')
                p = float(p_string) * 1000000000
            elif 'nghìn' in p_string:
                p_string = p_string.replace('nghìn', ' ')
                p = float(p_string) * 1000
            elif p_string != 'none' and p_string != 'thỏa thuận' and '萬/月' not in p_string and 'million' not in p_string:
                print('p 3 not valid: ' + str(p_string))
        else:
            if ('nghìn/m2' in p_string or 'nghìn/m²' in p_string) and a != None:
                p_string = p_string.replace('nghìn/m2', ' ')
                p_string = p_string.replace('nghìn/m²', ' ')
                p = float(p_string) * 1000 * a
            elif ('triệu/m2' in p_string or 'triệu/m²' in p_string) and a != None:
                p_string = p_string.replace('triệu/m2', ' ')
                p_string = p_string.replace('triệu/m²', ' ')
                p = float(p_string) * 1000000 * a
            elif ('tỷ/m2' in p_string or 'tỷ/m²' in p_string) and a != None:
                p_string = p_string.replace('tỷ/m2', ' ')
                p_string = p_string.replace('tỷ/m²', ' ')
                p = float(p_string) * 1000000000 * a
            elif p_string != 'none' and a != None:
                print('p 4 not valid: ' + str(p_string))
    if p!=None and type_p==1:
        if p < 100e6:
            type_p = 0


    list_a_range = [(0, 30), (30, 50), (50, 80), (80, 100), (100, 150), (150, 200), (200, 250), (250, 300), (300, 500),
                    (500, 100000000)]

    if a != None:
        for i in range(len(list_a_range)):
            pair = list_a_range[i]
            a1 = pair[0]
            a2 = pair[1]
            if a > a1 and a <= a2:
                a_id = i
                break

    list_p0_range = [(0, 1), (1, 3), (3, 5), (5, 10), (10, 40), (40, 70), (70, 100), (100, 1000000000)]
    list_p1_range = [(0, 500), (500, 800), (800, 1000), (1000, 2000), (2000, 3000), (3000, 5000), (5000, 7000),
                     (7000, 10000), (10000, 20000), (20000, 30000), (30000, 1000000000)]
    if p != None:
        if type_p == 0:
            for i in range(len(list_p0_range)):
                pair = list_p0_range[i]
                p1 = pair[0]
                p2 = pair[1]
                if p >= p1 * 1e6 and p < p2 * 1e6:
                    p_id = i
                    break
        elif type_p == 1:
            based_index = len(list_p0_range)
            for i in range(len(list_p1_range)):
                pair = list_p1_range[i]
                p1 = pair[0]
                p2 = pair[1]
                if p >= p1 * 1e6 and p < p2 * 1e6:
                    p_id = i + based_index
                    break

    return p_id, a_id
