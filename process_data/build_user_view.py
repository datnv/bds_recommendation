import sys

sys.path.append("../")
from operator import itemgetter
from process_data.helper import *
from process_data.helper import *
import os
from cf_cb_recommendation.rc_input import convert_page_to_list_indexes

#num_top_user = 10000

dic_cityid = {}
dic_cateid = {}
dic_districtid = {}
dic_projectid = {}
dic_streetid = {}
dic_wardid = {}
dic_priceid = {}
dic_areaid = {}

dic_cityid_inverse = {}
dic_cateid_inverse = {}
dic_districtid_inverse = {}
dic_projectid_inverse = {}
dic_streetid_inverse = {}
dic_wardid_inverse = {}
dic_priceid_inverse = {}
dic_areaid_inverse = {}


cityid = 0
cateid = 0
districtid = 0
projectid = 0
streetid = 0
wardid = 0
priceid = 0
areaid = 0

dic_user = {}
list_user = []
if not os.path.exists('file npy/dic_user.npy'):
    i = 0
    with open('file txt/data.txt', encoding='utf-8') as f1:
        for line in f1:
            line = line.strip().split('\t')
            if len(line) == 10:
                if i % 1000000 == 0:
                    print(i)
                i += 1
                user_session_id = line[0]
                time = line[1]
                cc = line[2]
                ci = line[3]
                di = line[4]
                pi = line[5]
                si = line[6]
                wi = line[7]
                p = line[8]
                a = line[9]

                # edit p,a
                p, a = convert_pa_string_to_id(p, a)
                p = str(p)
                a = str(a)

                # page = [time,cc,ci,di,pi,si,wi,p,a]
                page = [time, cc, ci, di, pi, si, wi, p, a]
                # print(page)
                if user_session_id not in dic_user:
                    dic_user[user_session_id] = []
                    list_user.append(user_session_id)
                dic_user[user_session_id].append(page)
    # save_dic(dic_user,'file npy/dic_user.npy')
# else:
#    dic_user = load_dic('file npy/dic_user.npy')

# find thresoft num pages
list_num_pages = []
with open('file txt/user_view_train.txt', 'w', encoding='utf-8') as f_train, open('file txt/user_view_val.txt', 'w',
                                                                                  encoding='utf-8') as f_test:
    for user_session_id in dic_user.keys():
        pages = dic_user[user_session_id]
        sorted(pages, key=itemgetter(0))
        dic_count_pages = {}
        new_pages = []
        for page in pages:
            page = page[1::]
            page = '::'.join(page)
            if page in dic_count_pages:
                dic_count_pages[page] += 1
            else:
                dic_count_pages[page] = 1
                new_pages.append(page)

        list_num_pages.append(len(new_pages))
list_num_pages.sort(reverse=True)
# start min num page
#min_num_page = 3
#index_of_min = list_num_pages.index(min_num_page-1)-1
#min_num_page = list_num_pages[num_top_user]
#max_num_page = list_num_pages[0]
#max_num_page = 3
#print('min_num_page: ' + str(min_num_page))
#print('max_num_page: ' + str(max_num_page))
# end min num page

#thresoft_num_page = list_num_pages[num_top_user-1]
#print('thresoft_num_page: '+str(thresoft_num_page))

#### build axilary dictionary
'''
with open('file csv/cities.csv',encoding='utf-8') as file:
    rowid = -1
    cityid = 0
    for line in file:
        rowid += 1
        if rowid == 0:
            continue
        row = line.strip().split(',')
        citycode = row[0]
        dic_cityid[citycode] = cityid
        dic_cityid_inverse[cityid] = citycode
        cityid+=1

with open('file csv/cates.csv',encoding='utf-8') as file:
    rowid = -1
    cateid = 0
    for line in file:
        rowid += 1
        if rowid == 0:
            continue
        row = line.strip().split(',')
        cateid_txt = int(row[0])
        dic_cateid[cateid_txt] = cateid
        dic_cateid_inverse[cateid] = cateid_txt
        cateid+=1

with open('file csv/districts.csv',encoding='utf-8') as file:
    rowid = -1
    districtid = 0
    for line in file:
        rowid += 1
        if rowid == 0:
            continue
        row = line.strip().split(',')
        districtid_txt = int(row[0])
        dic_districtid[districtid_txt] = districtid
        dic_districtid_inverse[districtid] = districtid_txt
        districtid+=1

with open('file csv/projects.csv',encoding='utf-8') as file:
    rowid = -1
    projectid = 0
    for line in file:
        rowid += 1
        if rowid == 0:
            continue
        row = line.strip().split(',')
        projectid_txt = int(row[0])
        dic_projectid[projectid_txt] = projectid
        dic_projectid_inverse[projectid] = projectid_txt
        projectid+=1

with open('file csv/streets.csv',encoding='utf-8') as file:
    rowid = -1
    streetid = 0
    for line in file:
        rowid += 1
        if rowid == 0:
            continue
        row = line.strip().split(',')
        streetid_txt = int(row[0])
        dic_streetid[streetid_txt] = streetid
        dic_streetid_inverse[streetid] = streetid_txt
        streetid+=1

with open('file csv/wards.csv',encoding='utf-8') as file:
    rowid = -1
    wardid = 0
    for line in file:
        rowid += 1
        if rowid == 0:
            continue
        row = line.strip().split(',')
        wardid_txt = int(row[0])
        dic_wardid[wardid_txt] = wardid
        dic_wardid_inverse[wardid] = wardid_txt
        wardid+=1

for i in range(19):
    dic_priceid[i] = i
    dic_priceid_inverse[i]=i

for i in range(10):
    dic_areaid[i] = i
    dic_areaid_inverse[i]=i

'''
#### end build axilary dictionary


dic_map_usi_to_userid = {}
dic_map_userid_to_usi = {}
current_user_id = 0
dic_item = {}
num_cell_postive = 0

with open('file txt/user_view_train_origin.txt', 'w', encoding='utf-8') as f_train, open(
        'file txt/user_view_val_origin.txt', 'w', encoding='utf-8') as f_test:
    #for user_session_id in dic_user.keys():
    for user_session_id in list_user:
        pages = dic_user[user_session_id]
        sorted(pages, key=itemgetter(0))
        dic_count_pages = {}
        new_pages = []
        for page in pages:
            page = page[1::]
            page = '::'.join(page)
            if page in dic_count_pages:
                dic_count_pages[page] += 1
            else:
                dic_count_pages[page] = 1
                new_pages.append(page)

        if len(new_pages) > 0:
        #if len(new_pages) >= min_num_page and len(new_pages) <= max_num_page:
            pages = new_pages
            dic_map_usi_to_userid[user_session_id] = current_user_id
            dic_map_userid_to_usi[current_user_id] = user_session_id
            current_user_id += 1
            #if current_user_id > num_top_user:
            #    break
            num_cell_postive += len(pages)

            # build auxilary dic
            for page in pages:
                if page not in dic_item:
                    dic_item[page] = 1

                cc, ci, di, pi, si, wi, p, a = tuple(page.split('::'))
                if cc != 'None' and cc not in dic_cityid:
                    dic_cityid[cc] = cityid
                    dic_cityid_inverse[cityid] = cc
                    cityid += 1
                if ci != 'None' and int(ci) not in dic_cateid:
                    dic_cateid[int(ci)] = cateid
                    dic_cateid_inverse[cateid] = int(ci)
                    cateid += 1
                if di != 'None' and int(di) not in dic_districtid:
                    dic_districtid[int(di)] = districtid
                    dic_districtid_inverse[districtid] = int(di)
                    districtid += 1
                if pi != 'None' and int(pi) not in dic_projectid:
                    dic_projectid[int(pi)] = projectid
                    dic_projectid_inverse[projectid] = int(pi)
                    projectid += 1
                if si != 'None' and int(si) not in dic_streetid:
                    dic_streetid[int(si)] = streetid
                    dic_streetid_inverse[streetid] = int(si)
                    streetid += 1
                if wi != 'None' and int(wi) not in dic_wardid:
                    dic_wardid[int(wi)] = wardid
                    dic_wardid_inverse[wardid] = int(wi)
                    wardid += 1
                if p != 'None' and int(p) not in dic_priceid:
                    dic_priceid[int(p)] = priceid
                    dic_priceid_inverse[priceid] = int(p)
                    priceid += 1
                if a != 'None' and int(a) not in dic_areaid:
                    dic_areaid[int(a)] = areaid
                    dic_areaid_inverse[areaid] = int(a)
                    areaid += 1

            pages_train = pages
            pages_test = []
            if len(pages) > 10:
                pages_train = pages[:int(len(pages) * 1.0):]
                pages_test = pages[int(len(pages) * 1.0)::]
                f_test.write(str(user_session_id) + '****')
            f_train.write(str(user_session_id) + '****')

            for page in pages_train:
                f_train.write(str(dic_count_pages[page]) + '::' + page)
                f_train.write('||||')
            for page in pages_test:
                f_test.write(str(dic_count_pages[page]) + '::' + page)
                f_test.write('||||')
            f_train.write('\n')
            if len(pages) > 10:
                f_test.write('****')
                for page in pages_train:
                    f_test.write(str(dic_count_pages[page]) + '::' + page)
                    f_test.write('||||')
                f_test.write('\n')

save_dic(dic_map_usi_to_userid, 'file npy/dic_map_usi_to_userid.npy')
save_dic(dic_map_userid_to_usi, 'file npy/dic_map_userid_to_usi.npy')
save_dic(dic_item, 'file npy/dic_item.npy')

print('number user: ' + str(len(dic_map_usi_to_userid.keys())))
print('num_cell_postive: ' + str(num_cell_postive))
print('number of item: ' + str(len(dic_item.keys())))

len_vector_item = {'dic_cityid': len(dic_cityid), 'dic_cateid': len(dic_cateid),
                                'dic_districtid': len(dic_districtid), 'dic_projectid': len(dic_projectid),
                                'dic_streetid': len(
                                    dic_streetid), 'dic_wardid': len(dic_wardid), 'dic_priceid': len(dic_priceid),
                                'dic_areaid': len(dic_areaid)}

num_attribute = len(dic_cityid) + len(dic_cateid) + len(dic_districtid) + len(dic_projectid) + len(
    dic_streetid) + len(
    dic_wardid) + len(dic_priceid) + len(dic_areaid)

#build data id form
dic_item_id_form = {}
dic_item_id_index = 0
with open('file txt/user_view_train_origin.txt', 'r', encoding='utf-8') as f_train_origin, open(
        'file txt/user_view_train.txt', 'w', encoding='utf-8') as f_train:
    for line in f_train_origin:
        line = line.strip()
        line = line.split('****')
        user_id = dic_map_usi_to_userid[line[0]]
        f_train.write(str(user_id)+'****')
        line = line[1].split('||||')
        line = line[:len(line) - 1:]
        for page in line:
            page = page.split('::')
            r = int(page[0])
            page = page[1::]
            item_vector = convert_page_to_list_indexes(page, dic_cityid, dic_cateid, dic_districtid,
                                                       dic_projectid, dic_streetid,
                                                       dic_wardid, dic_priceid, dic_areaid,
                                                       len_vector_item, num_attribute)
            if '::'.join(item_vector) not in dic_item_id_form:
                dic_item_id_form['::'.join(item_vector)] = dic_item_id_index
                dic_item_id_index+=1
            f_train.write(str(r)+'::'+str(dic_item_id_form['::'.join(item_vector)]))
            f_train.write('||||')
        f_train.write('\n')




with open('file txt/user_view_val_origin.txt', 'r', encoding='utf-8') as f_test_origin, open(
        'file txt/user_view_val.txt', 'w', encoding='utf-8') as f_test:
    for line in f_test_origin:
        line = line.strip()
        line = line.split('****')
        user_id = dic_map_usi_to_userid[line[0]]
        f_test.write(str(user_id) + '****')
        line_val = line[1].split('||||')
        line_val = line_val[:len(line_val) - 1:]
        line_train = line[2].split('||||')
        line_train = line_train[:len(line_train) - 1:]
        for page in line_val:
            page = page.split('::')
            r = int(page[0])
            page = page[1::]
            item_vector = convert_page_to_list_indexes(page, dic_cityid, dic_cateid, dic_districtid,
                                                       dic_projectid, dic_streetid,
                                                       dic_wardid, dic_priceid, dic_areaid,
                                                       len_vector_item, num_attribute)
            if '::'.join(item_vector) not in dic_item_id_form:
                dic_item_id_form['::'.join(item_vector)] = dic_item_id_index
                dic_item_id_index+=1
            f_test.write(str(r)+'::'+str(dic_item_id_form['::'.join(item_vector)]))
            f_test.write('||||')
        f_test.write('****')
        for page in line_train:
            page = page.split('::')
            r = int(page[0])
            page = page[1::]
            item_vector = convert_page_to_list_indexes(page, dic_cityid, dic_cateid, dic_districtid,
                                                       dic_projectid, dic_streetid,
                                                       dic_wardid, dic_priceid, dic_areaid,
                                                       len_vector_item, num_attribute)
            f_test.write(str(r)+'::'+str(dic_item_id_form['::'.join(item_vector)]))
            f_test.write('||||')
        f_test.write('\n')

list_all_pages = [0 for i in range(len(dic_item_id_form.keys()))]
for page in dic_item_id_form:
    ori_page = page
    page = page.split('::')
    item_vector = [int(page[i]) for i in range(len(page))]
    index = dic_item_id_form[ori_page]
    list_all_pages[index] = item_vector

save_dic(dic_item_id_form,'file npy/dic_item_id_form.npy')
np.save('file npy/list_all_pages.npy',list_all_pages)


print('save dictionaries ...')
save_dic(dic_cityid, 'file npy/dic_cityid.npy')
save_dic(dic_cateid, 'file npy/dic_cateid.npy')
save_dic(dic_districtid, 'file npy/dic_districtid.npy')
save_dic(dic_projectid, 'file npy/dic_projectid.npy')
save_dic(dic_streetid, 'file npy/dic_streetid.npy')
save_dic(dic_wardid, 'file npy/dic_wardid.npy')
save_dic(dic_priceid, 'file npy/dic_priceid.npy')
save_dic(dic_areaid, 'file npy/dic_areaid.npy')

save_dic(dic_cityid_inverse, 'file npy/dic_cityid_inverse.npy')
save_dic(dic_cateid_inverse, 'file npy/dic_cateid_inverse.npy')
save_dic(dic_districtid_inverse, 'file npy/dic_districtid_inverse.npy')
save_dic(dic_projectid_inverse, 'file npy/dic_projectid_inverse.npy')
save_dic(dic_streetid_inverse, 'file npy/dic_streetid_inverse.npy')
save_dic(dic_wardid_inverse, 'file npy/dic_wardid_inverse.npy')
save_dic(dic_priceid_inverse, 'file npy/dic_priceid_inverse.npy')
save_dic(dic_areaid_inverse, 'file npy/dic_areaid_inverse.npy')
