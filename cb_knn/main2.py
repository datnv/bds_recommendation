import math
import time
from process_data.helper import *
import scipy.sparse
import numpy as np

def compute_rank_in_batch(train_matrix, test_matrix, list_r_test_batch, list_id_test_batch, list_id_train_batch):
    rank = 0
    weight_sum = 0
    test_matrix = test_matrix.transpose()
    #score matrix his a matrix with size (num_user_per_batch,num_all_pages)
    score_matrix = np.array(train_matrix.dot(test_matrix).todense())
    for i in range(np.shape(score_matrix)[0]):
        list_id_train = list_id_train_batch[i]
        score_vector = score_matrix[i,:].flatten()
        #print(score_vector)
        #print(len(list_id_train))
        #score_vector = [score_vector[i] for i in range(len(score_vector)) if score_vector[i] not in list_id_train ]
        #print(len(score_vector))

        list_r_test = list_r_test_batch[i]
        list_id_test = list_id_test_batch[i]

        index_sort = sorted(range(len(score_vector)), key=lambda k: score_vector[k], reverse=True)
        index_sort = [index_sort[i] for i in range(len(index_sort)) if index_sort[i] not in list_id_train]
        #print(len(index_sort))
        for j in range(len(list_r_test)):
            id = list_id_test[j]
            r = list_r_test[j]
            index = index_sort.index(id)
            sub_rank = index / float(len(index_sort))
            rank += r * sub_rank
            weight_sum += r
    return rank,weight_sum


def convert_page_to_list_indexes(page, dic_cityid, dic_cateid, dic_districtid, dic_projectid, dic_streetid,
                                 dic_wardid, dic_priceid, dic_areaid, len_vector_item, num_attribute):
    list_j = []
    list_pair = [('dic_cityid', dic_cityid), ('dic_cateid', dic_cateid), ('dic_districtid', dic_districtid),
                 ('dic_projectid', dic_projectid), ('dic_streetid', dic_streetid), ('dic_wardid', dic_wardid),
                 ('dic_priceid', dic_priceid), ('dic_areaid', dic_areaid)]
    # vector = [num_attribute for i in range(len(list_pair))]
    base_index = 0
    for i, pair in enumerate(list_pair):
        # print(i)
        # print(pair)
        name_dic = pair[0]
        dic = pair[1]
        if page[i] != 'None':
            if page[i].isdigit():
                page[i] = int(page[i])
            # print(page[i])
            # try:
            id = dic[page[i]] + base_index
            # vector[i] = id
            list_j.append(id)
        base_index += len_vector_item[name_dic]
    return list_j



def compute_rank(file_test, list_all_pages, num_user_per_batch, dic_cityid, dic_cateid, dic_districtid,
                 dic_projectid, dic_streetid,
                 dic_wardid, dic_priceid, dic_areaid,
                 len_vector_item, num_attribute):
    RANK = 0
    WEIGHT_SUM = 0
    num_batch = 0
    start_time = time.time()

    train_matrix_data = []
    train_matrix_i = []
    train_matrix_j = []
    test_matrix_data = []
    test_matrix_i = []
    test_matrix_j = []
    n = 0
    row_id_test = 0
    list_r_test_batch = []
    list_id_test_batch = []
    dic_view_page_val = {}
    list_dic_view_page_train = []
    with open(file_test, mode='r', encoding='utf-8') as f1:
        for k, line in enumerate(f1):
            #if k == 5:
            #    break
            #list_r_train = []
            #list_r_test = []
            line = line.strip()
            line = line.split('****')
            # user_id = dic_map_usi_to_userid[line[0]]
            line_val = line[1].split('||||')
            line_val = line_val[:len(line_val) - 1:]
            line_train = line[2].split('||||')
            line_train = line_train[:len(line_train) - 1:]



            #train_data
            dic_j_to_train_data_user = {}
            sum_r_user = 0
            dic_view_page_train = {}
            for i in range(len(line_train)):
                page = line_train[i].split('::')
                r = int(page[0])
                sum_r_user += r
                page = page[1::]
                ori_page = list(page)
                dic_view_page_train['::'.join(page)] = 1
                list_j = convert_page_to_list_indexes(page, dic_cityid, dic_cateid, dic_districtid,
                                                      dic_projectid, dic_streetid,
                                                      dic_wardid, dic_priceid, dic_areaid,
                                                      len_vector_item, num_attribute)
                if math.sqrt(len(list_j)) == 0:
                    weight = 0
                else:
                    weight = float(r) / math.sqrt(len(list_j))
                    #weight = 1. / math.sqrt(len(list_j))
                for j in list_j:
                    if j in dic_j_to_train_data_user:
                        dic_j_to_train_data_user[j] +=  weight
                    else:
                        dic_j_to_train_data_user[j] = weight
            list_dic_view_page_train.append(dic_view_page_train)
            list_j_train_user = []
            list_i_train_user = []
            list_data_train_user = []
            for j in dic_j_to_train_data_user:
                list_j_train_user.append(j)
                list_data_train_user.append(dic_j_to_train_data_user[j]/float(sum_r_user))
                #list_data_train_user.append(dic_j_to_train_data_user[j])
                list_i_train_user.append(n)

            train_matrix_j.extend(list_j_train_user)
            train_matrix_i.extend(list_i_train_user)
            train_matrix_data.extend(list_data_train_user)

            #test_data
            list_r_test = []
            list_id_test = []
            for i in range(len(line_val)):
                page = line_val[i].split('::')
                r = int(page[0])
                page = page[1::]
                ori_page = list(page)
                if '::'.join(ori_page) not in dic_view_page_val:
                    list_j = convert_page_to_list_indexes(page, dic_cityid, dic_cateid, dic_districtid,
                                                          dic_projectid, dic_streetid,
                                                          dic_wardid, dic_priceid, dic_areaid,
                                                          len_vector_item, num_attribute)
                    if math.sqrt(len(list_j))== 0 :
                        weight = 0
                    else:
                        weight = 1. / math.sqrt(len(list_j))
                    list_i = [row_id_test for k in range(len(list_j))]
                    list_data = [weight for k in range(len(list_j))]
                    test_matrix_j.extend(list_j)
                    test_matrix_i.extend(list_i)
                    test_matrix_data.extend(list_data)
                    dic_view_page_val['::'.join(ori_page)] = row_id_test
                    list_id_test.append(row_id_test)
                    row_id_test += 1
                else:
                    list_id_test.append(dic_view_page_val['::'.join(ori_page)])
                list_r_test.append(r)
            list_r_test_batch.append(list_r_test)
            list_id_test_batch.append(list_id_test)
            n+=1

            if n == num_user_per_batch:

                #add pages to test matrix
                for page in list_all_pages:
                    if page not in dic_view_page_val:
                        page = page.split('::')
                        ori_page = list(page)
                        r = 0
                        list_j = convert_page_to_list_indexes(page, dic_cityid, dic_cateid,
                                                              dic_districtid,
                                                              dic_projectid, dic_streetid,
                                                              dic_wardid, dic_priceid, dic_areaid,
                                                              len_vector_item, num_attribute)
                        if math.sqrt(len(list_j))== 0 :
                            weight = 0
                        else:
                            weight = 1. / math.sqrt(len(list_j))
                        list_i = [row_id_test for k in range(len(list_j))]
                        dic_view_page_val['::'.join(ori_page)] = row_id_test
                        row_id_test += 1
                        list_data = [weight for k in range(len(list_j))]
                        test_matrix_j.extend(list_j)
                        test_matrix_i.extend(list_i)
                        test_matrix_data.extend(list_data)
                # find list_id_train_batch
                list_id_train_batch = []
                for i in range(len(list_dic_view_page_train)):
                    list_id_train = {}
                    dic_view_page_train = list_dic_view_page_train[i]
                    for page in dic_view_page_train.keys():
                        list_id_train[dic_view_page_val[page]]=1
                    list_id_train_batch.append(list_id_train)



                num_batch+=1
                print(num_batch)
                test_matrix = scipy.sparse.coo_matrix((test_matrix_data, (test_matrix_i, test_matrix_j)),
                                                      shape=(len(list_all_pages), num_attribute))
                train_matrix = scipy.sparse.coo_matrix((train_matrix_data, (train_matrix_i, train_matrix_j)),
                                                       shape=(num_user_per_batch, num_attribute))
                rank, weight_sum = compute_rank_in_batch(train_matrix, test_matrix, list_r_test_batch, list_id_test_batch, list_id_train_batch)

                RANK+=rank
                WEIGHT_SUM+=weight_sum

                train_matrix_data = []
                train_matrix_i = []
                train_matrix_j = []
                test_matrix_data = []
                test_matrix_i = []
                test_matrix_j = []
                n = 0
                row_id_test = 0
                list_r_test_batch = []
                list_id_test_batch = []
                dic_view_page_val = {}
                list_dic_view_page_train = []

    #start final
    if len(list_id_test_batch) > 0:
        for page in list_all_pages:
            if page not in dic_view_page_val:
                page = page.split('::')
                ori_page = list(page)
                r = 0
                list_j = convert_page_to_list_indexes(page, dic_cityid, dic_cateid,
                                                      dic_districtid,
                                                      dic_projectid, dic_streetid,
                                                      dic_wardid, dic_priceid, dic_areaid,
                                                      len_vector_item, num_attribute)
                if math.sqrt(len(list_j))== 0 :
                    weight = 0
                else:
                    weight = 1. / math.sqrt(len(list_j))
                list_i = [row_id_test for k in range(len(list_j))]
                dic_view_page_val['::'.join(ori_page)] = row_id_test
                row_id_test += 1
                list_data = [weight for k in range(len(list_j))]
                test_matrix_j.extend(list_j)
                test_matrix_i.extend(list_i)
                test_matrix_data.extend(list_data)
        # find list_id_train_batch
        list_id_train_batch = []
        for i in range(len(list_dic_view_page_train)):
            list_id_train = {}
            dic_view_page_train = list_dic_view_page_train[i]
            for page in dic_view_page_train.keys():
                list_id_train[dic_view_page_val[page]] = 1
            list_id_train_batch.append(list_id_train)

        num_batch+=1
        print(num_batch)
        test_matrix = scipy.sparse.coo_matrix((test_matrix_data, (test_matrix_i, test_matrix_j)),
                                              shape=(len(list_all_pages), num_attribute))
        train_matrix = scipy.sparse.coo_matrix((train_matrix_data, (train_matrix_i, train_matrix_j)),
                                               shape=(len(list_id_train_batch), num_attribute))
        rank, weight_sum = compute_rank_in_batch(train_matrix, test_matrix, list_r_test_batch, list_id_test_batch, list_id_train_batch)

        RANK+=rank
        WEIGHT_SUM+=weight_sum
    #end final



    end_time = time.time()
    print('--------------------------')
    print('rank in test: ' + str(RANK / float(WEIGHT_SUM)))
    print('time: ' + str(end_time - start_time))
    print('--------------------------')


if __name__ == '__main__':
    num_user_per_batch = 500
    file_test = '../process_data/file txt/user_view_val.txt'
    dic_item = load_dic('../process_data/file npy/dic_item.npy')
    list_all_pages = list(dic_item.keys())

    dic_cityid = load_dic('../process_data/file npy/dic_cityid.npy')
    dic_cateid = load_dic('../process_data/file npy/dic_cateid.npy')
    dic_districtid = load_dic('../process_data/file npy/dic_districtid.npy')
    dic_projectid = load_dic('../process_data/file npy/dic_projectid.npy')
    dic_streetid = load_dic('../process_data/file npy/dic_streetid.npy')
    dic_wardid = load_dic('../process_data/file npy/dic_wardid.npy')
    dic_priceid = load_dic('../process_data/file npy/dic_priceid.npy')
    dic_areaid = load_dic('../process_data/file npy/dic_areaid.npy')
    num_attribute = len(dic_cityid) + len(dic_cateid) + len(dic_districtid) + len(dic_projectid) + len(
        dic_streetid) + len(
        dic_wardid) + len(dic_priceid) + len(dic_areaid)
    len_vector_item = {'dic_cityid': len(dic_cityid), 'dic_cateid': len(dic_cateid),
                       'dic_districtid': len(dic_districtid), 'dic_projectid': len(dic_projectid),
                       'dic_streetid': len(
                           dic_streetid), 'dic_wardid': len(dic_wardid), 'dic_priceid': len(dic_priceid),
                       'dic_areaid': len(dic_areaid)}

    compute_rank(file_test, list_all_pages, num_user_per_batch, dic_cityid, dic_cateid, dic_districtid,
                 dic_projectid, dic_streetid,
                 dic_wardid, dic_priceid, dic_areaid,
                 len_vector_item, num_attribute)



