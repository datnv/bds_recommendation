import math
import time
from process_data.helper import *
import scipy.sparse

def cosine_similarity(page1,page2):
    a = 0
    l1 = 0
    l2 = 0
    for i in range(len(page1)):
        if page1[i]!='None' and page2[i]!='None' and page1[i]==page2[i]:
            a += 1
        if page1[i] != 'None':
            l1 += 1
        if page2[i] != 'None':
            l2 += 1
    if l1 == 0 or l2 == 0:
        return 0
    return float(a)/(math.sqrt(float(l1))*math.sqrt(float(l2)))

def compute_score(test_page, list_train_pages):
    a = 0
    b = 0
    for i in range(len(list_train_pages)):
        pair = list_train_pages[i]
        r = pair[0]
        train_page = pair[1]
        cos_sim = cosine_similarity(test_page,train_page)
        a += cos_sim*r
        b += r
    return float(a)/b

def compute_rank(file_test, list_all_pages, num_user_per_batch):
    rank = 0
    weight_sum = 0
    start_time = time.time()
    with open(file_test,mode='r',encoding='utf-8') as f1:
        for k,line in enumerate(f1):
            print(k)
            if k==5:
                break
            list_item_vector = []
            list_r = []
            line = line.strip()
            line = line.split('****')
            #user_id = dic_map_usi_to_userid[line[0]]
            line_val = line[1].split('||||')
            line_val = line_val[:len(line_val) - 1:]
            line_train = line[2].split('||||')
            line_train = line_train[:len(line_train) - 1:]
            dic_view_page_train = {}
            dic_view_page_val = {}
            list_train_pages = []
            for i in range(len(line_val)):
                page = line_val[i].split('::')
                r = int(page[0])
                page = page[1::]
                dic_view_page_val['::'.join(page)] = 1
                '''
                item_vector = convert_page_to_list_indexes(page, dic_cityid, dic_cateid, dic_districtid,
                                                           dic_projectid, dic_streetid,
                                                           dic_wardid, dic_priceid, dic_areaid,
                                                           len_vector_item, num_attribute)
                '''
                #list_user_id.append(user_id)
                list_item_vector.append(page)
                list_r.append(r)
            for i in range(len(line_train)):
                page = line_train[i].split('::')
                r = int(page[0])
                page = page[1::]
                list_train_pages.append((r,page))
                dic_view_page_train['::'.join(page)] = 1
            for page in list_all_pages:
                if page not in dic_view_page_val.keys() and page not in dic_view_page_train.keys():
                    page = page.split('::')
                    r = 0
                    '''
                    item_vector = convert_page_to_list_indexes(page, self.dic_cityid, self.dic_cateid,
                                                               self.dic_districtid,
                                                               self.dic_projectid, self.dic_streetid,
                                                               self.dic_wardid, self.dic_priceid, self.dic_areaid,
                                                               self.len_vector_item, self.num_attribute)
                    '''
                    #list_user_id.append(user_id)
                    list_item_vector.append(page)
                    list_r.append(r)
            s = []

            for i in range(len(list_item_vector)):
                score = compute_score(list_item_vector[i],list_train_pages)
                s.append(score)
            index_sort = sorted(range(len(s)), key=lambda k: s[k], reverse=True)
            for j in range(len(list_r)):
                if list_r[j] == 0:
                    break
                index = index_sort.index(j)
                sub_rank = index / float(len(index_sort))
                rank += list_r[j] * sub_rank
                weight_sum += list_r[j]

    end_time = time.time()
    print('--------------------------')
    print('rank in test: ' + str(rank / float(weight_sum)))
    print('time: ' + str(end_time - start_time))
    print('--------------------------')


if __name__ == '__main__':
    num_user_per_batch = 10
    file_test = '../process_data/file txt/user_view_val.txt'
    dic_item = load_dic('../process_data/file npy/dic_item.npy')
    list_all_pages = list(dic_item.keys())
    compute_rank(file_test,list_all_pages,num_user_per_batch)