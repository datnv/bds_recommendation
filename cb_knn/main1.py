import math
import time
from process_data.helper import *
import scipy.sparse
import numpy as np


def convert_page_to_list_indexes(page, dic_cityid, dic_cateid, dic_districtid, dic_projectid, dic_streetid,
                                 dic_wardid, dic_priceid, dic_areaid, len_vector_item, num_attribute):
    list_j = []
    list_pair = [('dic_cityid', dic_cityid), ('dic_cateid', dic_cateid), ('dic_districtid', dic_districtid),
                 ('dic_projectid', dic_projectid), ('dic_streetid', dic_streetid), ('dic_wardid', dic_wardid),
                 ('dic_priceid', dic_priceid), ('dic_areaid', dic_areaid)]
    # vector = [num_attribute for i in range(len(list_pair))]
    base_index = 0
    for i, pair in enumerate(list_pair):
        # print(i)
        # print(pair)
        name_dic = pair[0]
        dic = pair[1]
        if page[i] != 'None':
            if page[i].isdigit():
                page[i] = int(page[i])
            # print(page[i])
            # try:
            id = dic[page[i]] + base_index
            # vector[i] = id
            list_j.append(id)
        base_index += len_vector_item[name_dic]
    return list_j


def compute_rank_in_batch(train_matrix, test_matrix, list_r_train_batch, list_r_test_batch, list_real_len_train, list_real_len_test):
    assert train_matrix.get_shape()[0] == sum(list_real_len_train)
    assert test_matrix.get_shape()[0] == sum(list_real_len_test)
    #print(list_real_len_test)
    rank = 0
    weight_sum = 0
    test_matrix = test_matrix.transpose()
    DOT_MAT = train_matrix.dot(test_matrix).todense()
    LEN_VEC_TRAIN = np.sqrt(train_matrix.power(2).sum(axis=1)).reshape((train_matrix.get_shape()[0], 1))
    LEN_VEC_TEST = np.sqrt(test_matrix.power(2).sum(axis=0)).reshape((1, test_matrix.get_shape()[1]))
    for i in range(len(list_real_len_train)):
        #train = train_matrix[0+sum(list_real_len_train[:i:]):list_real_len_train[i]+sum(list_real_len_train[:i:]):,:]
        #test = test_matrix[0+sum(list_real_len_test[:i:]):list_real_len_test[i]+sum(list_real_len_test[:i:]):,:]
        list_r_train = list_r_train_batch[i]
        list_r_train = np.array(list_r_train).reshape((len(list_r_train), 1))
        sum_r_train = np.sum(list_r_train)
        list_r_test = list_r_test_batch[i]
        real_len_train = list_real_len_train[i]
        real_len_test = list_real_len_test[i]

        # compute cosine similarity
        #dot_mat = train.dot(test).todense()
        dot_mat = DOT_MAT[0+sum(list_real_len_train[:i:]):list_real_len_train[i]+sum(list_real_len_train[:i:]):,
                  0 + sum(list_real_len_test[:i:]):list_real_len_test[i] + sum(list_real_len_test[:i:]):]
        #len_vec_train = np.sqrt(train.power(2).sum(axis=1)).reshape((train_matrix.get_shape()[1], 1))
        len_vec_train = LEN_VEC_TRAIN[0+sum(list_real_len_train[:i:]):list_real_len_train[i]+sum(list_real_len_train[:i:]):,:]
        #len_vec_test = np.sqrt(test.power(2).sum(axis=0)).reshape((1, test_matrix.get_shape()[1]))
        len_vec_test = LEN_VEC_TEST[:,0 + sum(list_real_len_test[:i:]):list_real_len_test[i] + sum(list_real_len_test[:i:]):]
        cos_mat = dot_mat.astype(np.float) / np.dot(len_vec_train, len_vec_test)
        score = np.sum(np.multiply(cos_mat, list_r_train), axis=0) / sum_r_train
        score = score.reshape((np.shape(score)[1],1))
        #score = score.flatten()
        #print(np.shape(score))
        s = score[:real_len_test:]
        #print(s)
        index_sort = sorted(range(len(s)), key=lambda k: s[k], reverse=True)
        #print(index_sort)
        for j in range(len(list_r_test)):
            if list_r_test[j] == 0:
                break
            index = index_sort.index(j)
            sub_rank = index / float(len(index_sort))
            rank += list_r_test[j] * sub_rank
            weight_sum += list_r_test[j]
    return rank,weight_sum


def compute_rank(file_test, list_all_pages, num_user_per_batch, dic_cityid, dic_cateid, dic_districtid,
                 dic_projectid, dic_streetid,
                 dic_wardid, dic_priceid, dic_areaid,
                 len_vector_item, num_attribute):
    RANK = 0
    WEIGHT_SUM = 0
    num_batch = 0
    start_time = time.time()
    train_matrix_data = []
    train_matrix_n = []
    train_matrix_i = []
    train_matrix_j = []
    test_matrix_data = []
    test_matrix_n = []
    test_matrix_i = []
    test_matrix_j = []
    n = 0
    list_real_len_test = []
    list_real_len_train = []
    list_r_train_batch = []
    list_r_test_batch = []
    #max_view_page_train = 0
    row_id_train = 0
    row_id_test = 0
    with open(file_test, mode='r', encoding='utf-8') as f1:
        for k, line in enumerate(f1):
            if k == 5:
                break
            list_r_train = []
            list_r_test = []
            line = line.strip()
            line = line.split('****')
            # user_id = dic_map_usi_to_userid[line[0]]
            line_val = line[1].split('||||')
            line_val = line_val[:len(line_val) - 1:]
            line_train = line[2].split('||||')
            line_train = line_train[:len(line_train) - 1:]
            dic_view_page_train = {}
            dic_view_page_val = {}
            for i in range(len(line_val)):
                page = line_val[i].split('::')
                r = int(page[0])
                page = page[1::]
                dic_view_page_val['::'.join(page)] = 1
                list_j = convert_page_to_list_indexes(page, dic_cityid, dic_cateid, dic_districtid,
                                                      dic_projectid, dic_streetid,
                                                      dic_wardid, dic_priceid, dic_areaid,
                                                      len_vector_item, num_attribute)
                list_i = [row_id_test for k in range(len(list_j))]
                row_id_test+=1
                list_data = [1. for k in range(len(list_j))]
                #list_n = [n for k in range(len(list_j))]
                test_matrix_j.extend(list_j)
                test_matrix_i.extend(list_i)
                test_matrix_data.extend(list_data)
                #test_matrix_n.extend(list_n)
                list_r_test.append(r)
            for i in range(len(line_train)):
                page = line_train[i].split('::')
                r = int(page[0])
                page = page[1::]
                dic_view_page_train['::'.join(page)] = 1
                list_j = convert_page_to_list_indexes(page, dic_cityid, dic_cateid, dic_districtid,
                                                      dic_projectid, dic_streetid,
                                                      dic_wardid, dic_priceid, dic_areaid,
                                                      len_vector_item, num_attribute)
                list_i = [row_id_train for k in range(len(list_j))]
                row_id_train+=1
                list_data = [1. for k in range(len(list_j))]
                list_n = [n for k in range(len(list_j))]
                train_matrix_j.extend(list_j)
                train_matrix_i.extend(list_i)
                train_matrix_data.extend(list_data)
                #train_matrix_n.extend(list_n)
                list_r_train.append(r)
            #if max_view_page_train < len(dic_view_page_train.keys()):
            #    max_view_page_train = len(dic_view_page_train.keys())
            #based_test_index = len(line_val)
            id = 0
            for page in list_all_pages:
                if page not in dic_view_page_val.keys() and page not in dic_view_page_train.keys():
                    page = page.split('::')
                    r = 0
                    list_j = convert_page_to_list_indexes(page, dic_cityid, dic_cateid,
                                                          dic_districtid,
                                                          dic_projectid, dic_streetid,
                                                          dic_wardid, dic_priceid, dic_areaid,
                                                          len_vector_item, num_attribute)
                    list_i = [row_id_test for k in range(len(list_j))]
                    row_id_test+=1
                    list_data = [1. for k in range(len(list_j))]
                    list_n = [n for k in range(len(list_j))]
                    test_matrix_j.extend(list_j)
                    test_matrix_i.extend(list_i)
                    test_matrix_data.extend(list_data)
                    #test_matrix_n.extend(list_n)
                    list_r_test.append(r)
                    id += 1
            real_len_test = len(line_val) + id
            real_len_train = len(line_train)

            '''
            while len(list_user_id) < len(self.list_all_pages):
                r = 0
                list_user_id.append(user_id)
                list_item_vector.append([self.num_attribute for i in range(len(self.len_vector_item.keys()))])
                list_r.append(r)
            '''
            list_r_train_batch.append(list_r_train)
            list_r_test_batch.append(list_r_test)
            list_real_len_test.append(real_len_test)
            list_real_len_train.append(real_len_train)
            n += 1
            if n == num_user_per_batch:
                num_batch+=1
                print(num_batch)
                test_matrix = scipy.sparse.coo_matrix((test_matrix_data, (test_matrix_i, test_matrix_j)),
                                                      shape=(row_id_test, num_attribute))
                train_matrix = scipy.sparse.coo_matrix((train_matrix_data, (train_matrix_i, train_matrix_j)),
                                                       shape=(row_id_train, num_attribute))
                rank, weight_sum = compute_rank_in_batch(train_matrix, test_matrix, list_r_train_batch, list_r_test_batch,
                                      list_real_len_train, list_real_len_test)

                RANK+=rank
                WEIGHT_SUM+=weight_sum
                train_matrix_data = []
                train_matrix_n = []
                train_matrix_i = []
                train_matrix_j = []
                test_matrix_data = []
                test_matrix_n = []
                test_matrix_i = []
                test_matrix_j = []
                n = 0
                list_real_len_test = []
                list_real_len_train = []
                list_r_train_batch = []
                list_r_test_batch = []
                #max_view_page_train = 0
                row_id_train = 0
                row_id_test = 0

        test_matrix = scipy.sparse.coo_matrix((test_matrix_data, (test_matrix_i, test_matrix_j)),
                                              shape=(row_id_test, num_attribute))
        train_matrix = scipy.sparse.coo_matrix((train_matrix_data, (train_matrix_i, train_matrix_j)),
                                               shape=(row_id_train, num_attribute))
        rank, weight_sum = compute_rank_in_batch(train_matrix, test_matrix, list_r_train_batch,
                                                 list_r_test_batch,
                                                 list_real_len_train, list_real_len_test)

        RANK += rank
        WEIGHT_SUM += weight_sum

    end_time = time.time()
    print('--------------------------')
    print('rank in test: ' + str(RANK / float(WEIGHT_SUM)))
    print('time: ' + str(end_time - start_time))
    print('--------------------------')


if __name__ == '__main__':
    num_user_per_batch = 1
    file_test = '../process_data/file txt/user_view_val.txt'
    dic_item = load_dic('../process_data/file npy/dic_item.npy')
    list_all_pages = list(dic_item.keys())

    dic_cityid = load_dic('../process_data/file npy/dic_cityid.npy')
    dic_cateid = load_dic('../process_data/file npy/dic_cateid.npy')
    dic_districtid = load_dic('../process_data/file npy/dic_districtid.npy')
    dic_projectid = load_dic('../process_data/file npy/dic_projectid.npy')
    dic_streetid = load_dic('../process_data/file npy/dic_streetid.npy')
    dic_wardid = load_dic('../process_data/file npy/dic_wardid.npy')
    dic_priceid = load_dic('../process_data/file npy/dic_priceid.npy')
    dic_areaid = load_dic('../process_data/file npy/dic_areaid.npy')
    num_attribute = len(dic_cityid) + len(dic_cateid) + len(dic_districtid) + len(dic_projectid) + len(
        dic_streetid) + len(
        dic_wardid) + len(dic_priceid) + len(dic_areaid)
    len_vector_item = {'dic_cityid': len(dic_cityid), 'dic_cateid': len(dic_cateid),
                       'dic_districtid': len(dic_districtid), 'dic_projectid': len(dic_projectid),
                       'dic_streetid': len(
                           dic_streetid), 'dic_wardid': len(dic_wardid), 'dic_priceid': len(dic_priceid),
                       'dic_areaid': len(dic_areaid)}

    compute_rank(file_test, list_all_pages, num_user_per_batch, dic_cityid, dic_cateid, dic_districtid,
                 dic_projectid, dic_streetid,
                 dic_wardid, dic_priceid, dic_areaid,
                 len_vector_item, num_attribute)
