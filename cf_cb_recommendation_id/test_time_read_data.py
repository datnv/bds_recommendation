import time

import sys
sys.path.append("../")
from torch.utils.data import Dataset
from random import randint
from process_data.helper import *
from cf_cb_recommendation_id.parameters import *
import torch
from random import shuffle
import random

def convert_page_to_list_indexes(page, dic_cityid, dic_cateid, dic_districtid, dic_projectid, dic_streetid,
                                 dic_wardid, dic_priceid, dic_areaid, len_vector_item, num_attribute):
    list_pair = [('dic_cityid', dic_cityid), ('dic_cateid', dic_cateid), ('dic_districtid', dic_districtid),
                 ('dic_projectid', dic_projectid), ('dic_streetid', dic_streetid), ('dic_wardid', dic_wardid),
                 ('dic_priceid', dic_priceid), ('dic_areaid', dic_areaid)]
    #vector = [str(num_attribute) for i in range(len(list_pair))]
    vector = [str(num_attribute)] * len(list_pair)
    base_index = 0
    for i, pair in enumerate(list_pair):
        # print(i)
        # print(pair)
        name_dic = pair[0]
        dic = pair[1]
        if page[i] != 'None':
            if page[i].isdigit():
                page[i] = int(page[i])
            # print(page[i])
            try:
                id = dic[page[i]] + base_index
                vector[i] = str(id)
            except:
                if name_dic!='dic_projectid' and name_dic!='dic_streetid':
                    print('error dic '+str(name_dic)+' ; key: '+str(page[i]))
        base_index += len_vector_item[name_dic]
    return vector

def get_all_pages_id_attribute(num_attribute):
    list_pages = []
    for i in range(num_attribute):
        list_pages.append([i])
    return list_pages

def count_occur_attribute_all_pages(list_all_pages,num_attribute):
    result = [0 for i in range(num_attribute)]
    for item_vector in list_all_pages:
        for id in item_vector:
            if id < num_attribute:
                result[id]+=1
    return np.array(result)

def create_data(line, list_count_att_all, list_all_pages, num_attribute):
    line = line.strip()
    line = line.split('****')
    user_id = int(line[0])
    line = line[1].split('||||')
    line = line[:len(line) - 1:]
    list_count_att_user = np.array([0 for i in range(num_attribute)])
    list_count_att_user_binary = np.array([0 for i in range(num_attribute)])
    n = len(list_all_pages) - len(line)
    m = 0
    for i in range(len(line)):
        page = line[i].split('::')
        r = int(page[0])
        m += r
        index_page = int(page[1])
        item_vector = list_all_pages[index_page]
        for id in item_vector:
            if id < num_attribute:
                list_count_att_user[id] += r
                list_count_att_user_binary[id] += 1
    weight_list = list_count_att_user * n - m * (list_count_att_all - list_count_att_user_binary)
    weight_list = weight_list.tolist()
    list_user_id = [user_id] * num_attribute
    return weight_list, list_user_id


class rcoptimizeDatasets(Dataset):
    def __init__(self, file, dic_map_usi_to_userid, dic_item, dic_item_id_form, list_all_pages, num_negatives, num_page_per_user, dic_cityid, dic_cateid,
                 dic_districtid,
                 dic_projectid, dic_streetid, dic_wardid, dic_priceid, dic_areaid, num_attribute, type):
        with open(file, 'r') as file:
            self.lines = file.readlines()
        self.nSamples = len(self.lines)
        #self.dic_map_usi_to_userid = dic_map_usi_to_userid
        #self.dic_item = dic_item
        #self.num_negatives = num_negatives
        self.num_page_per_user = num_page_per_user
        self.list_all_pages = list_all_pages
        #self.dic_cityid = dic_cityid
        #self.dic_cateid = dic_cateid
        #self.dic_districtid = dic_districtid
        #self.dic_projectid = dic_projectid
        #self.dic_streetid = dic_streetid
        #self.dic_wardid = dic_wardid
        #self.dic_priceid = dic_priceid
        #self.dic_areaid = dic_areaid
        self.num_attribute = num_attribute
        self.len_vector_item = {'dic_cityid': len(dic_cityid), 'dic_cateid': len(dic_cateid),
                                'dic_districtid': len(dic_districtid), 'dic_projectid': len(dic_projectid),
                                'dic_streetid': len(
                                    dic_streetid), 'dic_wardid': len(dic_wardid), 'dic_priceid': len(dic_priceid),
                                'dic_areaid': len(dic_areaid)}
        self.all_pages_id_attribute = get_all_pages_id_attribute(num_attribute)
        self.list_count_att_all = count_occur_attribute_all_pages(self.list_all_pages,self.num_attribute)
        self.list_item_vector = [[i] for i in range(num_attribute)]
        self.type = type

    def __len__(self):
        return self.nSamples
        #return 50

    def __getitem__(self, index):
        list_user_id = []
        list_item_vector = []
        list_r = []
        if self.type == 'train':
            line = self.lines[index]
            list_r, list_user_id = create_data(line,self.list_count_att_all,self.list_all_pages,self.num_attribute)
            list_item_vector = self.list_item_vector

            list_user_id = list_user_id[:self.num_page_per_user:]
            list_item_vector = list_item_vector[:self.num_page_per_user:]
            list_r = list_r[:self.num_page_per_user:]
            return (torch.LongTensor(np.array(list_user_id, dtype=np.long)),
                    torch.LongTensor(np.array(list_item_vector, dtype=np.long)),
                    torch.IntTensor(np.array(list_r, dtype=np.int32)))
        elif self.type == 'val':
            line = self.lines[index].rstrip()
            line = line.split('****')
            user_id = int(line[0])
            line_val = line[1].split('||||')
            line_val = line_val[:len(line_val) - 1:]
            line_train = line[2].split('||||')
            line_train = line_train[:len(line_train) - 1:]
            dic_view_page_train = {}
            dic_view_page_val = {}
            for i in range(len(line_val)):
                page = line_val[i].split('::')
                r = int(page[0])
                index_page = int(page[1])
                dic_view_page_val[index_page] = 1
                item_vector = self.list_all_pages[index_page]
                #item_vector = [int(page[i]) for i in range(len(page))]
                list_user_id.append(user_id)
                list_item_vector.append(item_vector)
                list_r.append(r)
            for i in range(len(line_train)):
                page = line_train[i].split('::')
                index_page = int(page[1])
                dic_view_page_train[index_page] = 1
            for index_page in range(len(self.list_all_pages)):
                if index_page not in dic_view_page_val and index_page not in dic_view_page_train:
                    r = 0
                    item_vector = self.list_all_pages[index_page]
                    #item_vector = [int(page[i]) for i in range(len(page))]
                    list_user_id.append(user_id)
                    list_item_vector.append(item_vector)
                    list_r.append(r)
            real_len = [len(list_user_id)]
            while len(list_user_id) < len(self.list_all_pages):
                r = 0
                list_user_id.append(user_id)
                #list_item_vector.append([self.num_attribute for i in range(len(self.len_vector_item.keys()))])
                list_item_vector.append([self.num_attribute]*len(self.len_vector_item.keys()))
                list_r.append(r)
            # print(len(list_user_id))
            # print(len(list_item_vector))
            # print(len(list_r))
            # print(real_len)
            return (torch.LongTensor(np.array(list_user_id, dtype=np.long)),
                    torch.LongTensor(np.array(list_item_vector, dtype=np.long)),
                    torch.IntTensor(np.array(list_r, dtype=np.int32)),
                    torch.IntTensor(np.array(real_len)))
        elif self.type == 'recid':
            line = self.lines[index].rstrip()
            line = line.split('****')
            user_id = int(line[0])
            for page in self.all_pages_id_attribute:
                list_user_id.append(user_id)
                list_item_vector.append(page)
            return (torch.LongTensor(np.array(list_user_id, dtype=np.long)),
                    torch.LongTensor(np.array(list_item_vector, dtype=np.long)))


def get_dataloaders(dic_map_usi_to_userid, dic_item, dic_item_id_form, list_all_pages, dic_cityid, dic_cateid, dic_districtid, dic_projectid,
                    dic_streetid, dic_wardid, dic_priceid, dic_areaid, num_attribute):
    file_data = {}
    file_data['train'] = '../process_data/file txt/user_view_train.txt'
    file_data['val'] = '../process_data/file txt/user_view_val.txt'
    file_data['recid'] = '../process_data/file txt/user_view_train.txt'
    num_negatives = 0
    num_page_per_user = len(list_all_pages)
    rc_dataset = {
        x: rcoptimizeDatasets(file_data[x], dic_map_usi_to_userid, dic_item,
                              dic_item_id_form, list_all_pages, num_negatives, num_page_per_user,
                              dic_cityid, dic_cateid, dic_districtid,
                              dic_projectid, dic_streetid, dic_wardid, dic_priceid, dic_areaid, num_attribute, x) for x
        in ['train', 'val', 'recid']}

    '''
    rc_dataset = {
        x: rcDatasets('../process_data/file txt/user_view_' + x + '.txt', dic_map_usi_to_userid, dic_item,
                      num_negatives,
                      dic_cityid, dic_cateid, dic_districtid,
                      dic_projectid, dic_streetid, dic_wardid) for x in ['train', 'val']}
    '''

    dataloaders = {x: torch.utils.data.DataLoader(rc_dataset[x], batch_size[x],
                                                  shuffle=True, num_workers=1)
                   for x in ['train', 'val','recid']}
    return dataloaders

if __name__ == '__main__':
    dic_map_usi_to_userid = load_dic('../process_data/file npy/dic_map_usi_to_userid.npy')
    dic_item = load_dic('../process_data/file npy/dic_item.npy')
    dic_item_id_form = load_dic('../process_data/file npy/dic_item_id_form.npy')
    dic_cityid = load_dic('../process_data/file npy/dic_cityid.npy')
    dic_cateid = load_dic('../process_data/file npy/dic_cateid.npy')
    dic_districtid = load_dic('../process_data/file npy/dic_districtid.npy')
    dic_projectid = load_dic('../process_data/file npy/dic_projectid.npy')
    dic_streetid = load_dic('../process_data/file npy/dic_streetid.npy')
    dic_wardid = load_dic('../process_data/file npy/dic_wardid.npy')
    dic_priceid = load_dic('../process_data/file npy/dic_priceid.npy')
    dic_areaid = load_dic('../process_data/file npy/dic_areaid.npy')
    list_all_pages = np.load('../process_data/file npy/list_all_pages.npy')
    num_attribute = len(dic_cityid) + len(dic_cateid) + len(dic_districtid) + len(dic_projectid) + len(
        dic_streetid) + len(
        dic_wardid) + len(dic_priceid)+len(dic_areaid)

    dataloaders = get_dataloaders(dic_map_usi_to_userid, dic_item,dic_item_id_form, list_all_pages, dic_cityid, dic_cateid, dic_districtid,
                                           dic_projectid, dic_streetid, dic_wardid, dic_priceid, dic_areaid, num_attribute)
    start_time = time.time()
    end_time = 0
    for i, (batch_user_id, batch_item_vector, batch_r) in enumerate(dataloaders['train']):
        end_time = time.time()
        print('time to load a batch data: '+str(end_time-start_time))
        start_time = time.time()

