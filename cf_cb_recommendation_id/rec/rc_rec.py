import sys

sys.path.append("../../")
import torch
from cf_cb_recommendation_id.parameters import *
import cf_cb_recommendation_id.rc as rc
import cf_cb_recommendation_id.rec.rc_input_rec as rc_input_rec
import torch.optim as optim
from torch.optim import lr_scheduler
import shutil
import os
import time
from torch import nn
from random import shuffle
import cassandra
from cassandra.cluster import Cluster
import json
from datetime import datetime


def convert_page_to_list_indexes(page, dic_cityid, dic_cateid, dic_districtid, dic_projectid, dic_streetid,
                                 dic_wardid, dic_priceid, dic_areaid, len_vector_item, num_attribute):
    list_pair = [('dic_cityid', dic_cityid), ('dic_cateid', dic_cateid), ('dic_districtid', dic_districtid),
                 ('dic_projectid', dic_projectid), ('dic_streetid', dic_streetid), ('dic_wardid', dic_wardid),
                 ('dic_priceid', dic_priceid), ('dic_areaid', dic_areaid)]
    # vector = [str(num_attribute) for i in range(len(list_pair))]
    vector = [num_attribute] * len(list_pair)
    base_index = 0
    for i, pair in enumerate(list_pair):
        # print(i)
        # print(pair)
        name_dic = pair[0]
        dic = pair[1]
        if page[i] != 'NONE' and page[i] != -1:
            # if page[i].isdigit():
            #    page[i] = int(page[i])
            # print(page[i])
            try:
                id = dic[page[i]] + base_index
                vector[i] = id
            except:
                pass
            #    if name_dic!='dic_projectid' and name_dic!='dic_streetid':
            #        print('error dic '+str(name_dic)+' ; key: '+str(page[i]))
        base_index += len_vector_item[name_dic]
    return vector


def get_ori_page(page, list_pair):
    ori_page = []
    based_index = 0
    for i in range(len(list_pair)):
        dic_name = list_pair[i][0]
        len_dic = list_pair[i][1]
        dic_inverse = list_pair[i][2]
        if (page[i] - based_index) in dic_inverse:
            ori_id = dic_inverse[page[i] - based_index]
            ori_page.append(str(ori_id))
        else:
            ori_page.append('None')
        based_index += len_dic
    print(ori_page)
    return ori_page


def get_real_score(batch_item_vector, s):
    # add_id = torch.LongTensor([[[i*num_att]] for i in range(batch_item_vector.size()[0])]).to(device)
    # batch_item_vector = batch_item_vector + add_id
    s = s[batch_item_vector]
    s = torch.sum(s, dim=2)
    return s


def convert_page_to_json(page):
    rs = {'city_code': page[0], 'category_id': page[1], 'district_id': page[2], 'project_id': page[3],
          'street_id': page[4], 'ward_id': page[5], 'price_id': page[6], 'area_id': page[7]}
    rs = str(rs)
    return rs


def get_list_all_pages_from_cassandra(server, port):
    print('get list all pages from cassandra....')
    list_all_pages = []
    list_all_pages_json = []
    list_num_none = []
    cluster = Cluster([server], port=port)
    session = cluster.connect('recommendation')
    rows = session.execute(
        'select id,area_id,category_id,city_code,district_id,price_id,project_id,street_id,ward_id from recommendation.attribute ALLOW FILTERING;')
    for user_row in rows:
        id, area_id, category_id, city_code, district_id, price_id, project_id, street_id, ward_id = user_row.id, \
                                                                                                     user_row.area_id, user_row.category_id, user_row.city_code, user_row.district_id, \
                                                                                                     user_row.price_id, user_row.project_id, user_row.street_id, user_row.ward_id
        page = [city_code, category_id, district_id, project_id, street_id, ward_id, price_id, area_id]
        # print(page)
        item_vector = convert_page_to_list_indexes(page, dic_cityid, dic_cateid, dic_districtid, dic_projectid,
                                                   dic_streetid,
                                                   dic_wardid, dic_priceid, dic_areaid, len_vector_item, num_attribute)
        #print(item_vector)
        list_all_pages.append(item_vector)
        list_all_pages_json.append(convert_page_to_json(page))
        list_num_none.append(item_vector.count(num_attribute))
    index_sort = sorted(range(len(list_num_none)), key=lambda k: list_num_none[k], reverse=True)
    list_all_pages_json = np.array(list_all_pages_json)[index_sort]
    list_all_pages = np.array(list_all_pages)[index_sort].tolist()
    #print(np.array(list_num_none)[index_sort])
    print('done')
    return list_all_pages, list_all_pages_json


def get_batch_item_vector(batch_size, list_all_pages, num_attribute, device):
    # list_all_pages = list_all_pages.tolist()
    num_att = num_attribute + 1
    print('len(list_all_pages): ' + str(len(list_all_pages)))
    batch_item_vector = [list_all_pages] * batch_size
    batch_item_vector = torch.LongTensor(np.array(batch_item_vector, dtype=np.long)).to(device)
    add_id = torch.LongTensor([[[i * num_att]] for i in range(batch_item_vector.size()[0])]).to(device)
    batch_item_vector = batch_item_vector + add_id
    batch_item_vector = batch_item_vector
    return batch_item_vector


def get_batch_item_id_vector(batch_size, num_attribute, device):
    batch_item_id_vector = [[[i] for i in range(num_attribute + 1)]] * batch_size
    batch_item_id_vector = torch.LongTensor(np.array(batch_item_id_vector, dtype=np.long))
    batch_item_id_vector = batch_item_id_vector.to(device)
    batch_item_id_vector = batch_item_id_vector.view(batch_item_id_vector.size()[0] * batch_item_id_vector.size()[1],
                                                     batch_item_id_vector.size()[2])
    return batch_item_id_vector


def recommend_attribute_id(model, dataloaders, batch_item_vector, batch_item_id_vector, list_all_pages,
                           list_all_pages_json, server, port):
    print('load parameters')
    checkpoint = torch.load('../save/current/checkpoint.pth.tar')
    start_epoch = checkpoint['epoch']
    model.load_state_dict(checkpoint['state_dict'])
    print('epoch: ' + str(start_epoch))
    start_time = time.time()

    cluster = Cluster([server], port=port)
    session = cluster.connect('recommendation')
    insert_row_stmt = session.prepare("insert into recommendation.results(user_session_id,ids) values (?,?);")

    for i, (batch_user_id) in enumerate(dataloaders):
        batch_user_id_ori = batch_user_id
        #print(i)
        # if i==200:
        #    break
        ####
        batch_size = batch_user_id.size()[0]
        if batch_size != batch_item_vector.size()[0]:
            print('change bacth size.....')
            batch_item_vector = get_batch_item_vector(batch_size, list_all_pages, num_attribute, device)
            batch_item_id_vector = get_batch_item_id_vector(batch_size, num_attribute, device)
        num_page = batch_item_vector.size()[1]
        # num_att = batch_user_id.size()[1]
        batch_user_id = batch_user_id.view(batch_user_id.size()[0] * batch_user_id.size()[1])
        # batch_item_id_vector = batch_item_id_vector.view(batch_item_id_vector.size()[0] * batch_item_id_vector.size()[1],
        #                                               batch_item_id_vector.size()[2])
        batch_user_id = batch_user_id.to(device)
        # batch_item_vector = batch_item_vector.to(device)
        # batch_item_id_vector = batch_item_id_vector.to(device)
        with torch.set_grad_enabled(False):
            score_ori, score = model(batch_user_id, batch_item_id_vector)
            score = get_real_score(batch_item_vector, score)
            score = score.view(batch_size, num_page)
            sorted1, indices = torch.sort(score, descending=True)
            indices = indices.int()  # float, int save to numpy faster than long
            for i in range(batch_size):
                usi = dic_map_userid_to_usi[int(batch_user_id_ori[i][0].numpy())]
                # print(usi)
                # get_ori_page(list_item_vector[0],list_pair)
                # s = score[i]
                # s = s.cpu().numpy()

                index_sort = indices[i]
                index_sort = index_sort.cpu().numpy()[:24:]
                list_json = list_all_pages_json[index_sort].tolist()
                # index_sort = [str(id) for id in index_sort]
                # print(index_sort[:10:])
                session.execute(insert_row_stmt, (usi, list_json))

                # print(s)
                # index_sort = sorted(range(len(s)), key=lambda k: s[k], reverse=True)
                '''
                get_ori_page(list_item_vector[index_sort[0]],list_pair)
                print(s[index_sort[0]])
                get_ori_page(list_item_vector[index_sort[1]],list_pair)
                print(s[index_sort[1]])
                get_ori_page(list_item_vector[index_sort[2]],list_pair)
                print(s[index_sort[2]])
                get_ori_page(list_item_vector[index_sort[3]],list_pair)
                print(s[index_sort[3]])
                print('---------------------')
                '''
                '''
                s = score_ori[i]
                r = batch_r[i]
                s = s[:batch_real_len[i]:]
                r = r[:batch_real_len[i]:]
                s = s.cpu().numpy()
                r = r.cpu().numpy()
                print(s)
                index_sort = sorted(range(len(s)), key=lambda k: s[k], reverse=True)
                get_ori_page(list_item_vector[index_sort[0]],list_pair)
                print(s[index_sort[0]])
                get_ori_page(list_item_vector[index_sort[1]],list_pair)
                print(s[index_sort[1]])
                get_ori_page(list_item_vector[index_sort[2]],list_pair)
                print(s[index_sort[2]])
                get_ori_page(list_item_vector[index_sort[3]],list_pair)
                print(s[index_sort[3]])
                print('=====================')
                '''
    end_time = time.time()
    print('time: ' + str(end_time - start_time))


if __name__ == '__main__':
    batch_size = 50

    server = '172.16.0.11'
    port = 9042

    dic_map_usi_to_userid = load_dic('../../process_data/file npy/dic_map_usi_to_userid.npy')
    dic_map_userid_to_usi = load_dic('../../process_data/file npy/dic_map_userid_to_usi.npy')
    dic_item = load_dic('../../process_data/file npy/dic_item.npy')
    dic_item_id_form = load_dic('../../process_data/file npy/dic_item_id_form.npy')
    dic_cityid = load_dic('../../process_data/file npy/dic_cityid.npy')
    dic_cateid = load_dic('../../process_data/file npy/dic_cateid.npy')
    dic_districtid = load_dic('../../process_data/file npy/dic_districtid.npy')
    dic_projectid = load_dic('../../process_data/file npy/dic_projectid.npy')
    dic_streetid = load_dic('../../process_data/file npy/dic_streetid.npy')
    dic_wardid = load_dic('../../process_data/file npy/dic_wardid.npy')
    dic_priceid = load_dic('../../process_data/file npy/dic_priceid.npy')
    dic_areaid = load_dic('../../process_data/file npy/dic_areaid.npy')

    dic_cityid_inverse = load_dic('../../process_data/file npy/dic_cityid_inverse.npy')
    dic_cateid_inverse = load_dic('../../process_data/file npy/dic_cateid_inverse.npy')
    dic_districtid_inverse = load_dic('../../process_data/file npy/dic_districtid_inverse.npy')
    dic_projectid_inverse = load_dic('../../process_data/file npy/dic_projectid_inverse.npy')
    dic_streetid_inverse = load_dic('../../process_data/file npy/dic_streetid_inverse.npy')
    dic_wardid_inverse = load_dic('../../process_data/file npy/dic_wardid_inverse.npy')
    dic_priceid_inverse = load_dic('../../process_data/file npy/dic_priceid_inverse.npy')
    dic_areaid_inverse = load_dic('../../process_data/file npy/dic_areaid_inverse.npy')

    len_vector_item = {'dic_cityid': len(dic_cityid), 'dic_cateid': len(dic_cateid),
                       'dic_districtid': len(dic_districtid), 'dic_projectid': len(dic_projectid),
                       'dic_streetid': len(
                           dic_streetid), 'dic_wardid': len(dic_wardid), 'dic_priceid': len(dic_priceid),
                       'dic_areaid': len(dic_areaid)}
    num_attribute = len(dic_cityid) + len(dic_cateid) + len(dic_districtid) + len(dic_projectid) + len(
        dic_streetid) + len(
        dic_wardid) + len(dic_priceid) + len(dic_areaid)
    # list_all_pages = np.load('../../process_data/file npy/list_all_pages.npy')
    list_all_pages, list_all_pages_json = get_list_all_pages_from_cassandra(server, port)
    print('len list all pages: ' + str(len(list_all_pages)))

    '''
    list_pair = [('dic_cityid', len(dic_cityid), dic_cityid_inverse),
                 ('dic_cateid', len(dic_cateid), dic_cateid_inverse),
                 ('dic_districtid', len(dic_districtid), dic_districtid_inverse),
                 ('dic_projectid', len(dic_projectid), dic_projectid_inverse),
                 ('dic_streetid', len(dic_streetid), dic_streetid_inverse),
                 ('dic_wardid', len(dic_wardid), dic_wardid_inverse),
                 ('dic_priceid', len(dic_priceid), dic_priceid_inverse),
                 ('dic_areaid', len(dic_areaid), dic_areaid_inverse)]
    '''
    use_cuda = use_cuda and torch.cuda.is_available()
    device = torch.device("cuda:0" if use_cuda else "cpu")

    model = rc.optimizeNet(len(dic_map_usi_to_userid.keys()), factor_dim, num_attribute)

    model = model.to(device)

    print(model)

    dataloaders = rc_input_rec.get_dataloaders(dic_map_userid_to_usi, num_attribute, batch_size)

    batch_item_vector = get_batch_item_vector(batch_size, list_all_pages, num_attribute, device)
    batch_item_id_vector = get_batch_item_id_vector(batch_size, num_attribute, device)

    recommend_attribute_id(model, dataloaders, batch_item_vector, batch_item_id_vector, list_all_pages,
                           list_all_pages_json, server, port)

    print(datetime.now())
    print('\n================================================\n')
