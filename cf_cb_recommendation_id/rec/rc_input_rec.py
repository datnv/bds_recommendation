import torch
from torch.utils.data import Dataset
import numpy as np

class rcoptimizeDatasets(Dataset):
    def __init__(self, dic_map_userid_to_usi, num_attribute):
        self.list_all_user_id = list(dic_map_userid_to_usi.keys())#
        self.nSamples = len(self.list_all_user_id)
        self.num_attribute = num_attribute
        #self.batch_all_user_id = self.create_batch_user_id(self.list_all_user_id)
        #with open(file, 'r') as file:
        #    self.lines = file.readlines()
        #self.nSamples = len(self.lines)
        #self.dic_map_usi_to_userid = dic_map_usi_to_userid
        #self.dic_item = dic_item
        #self.num_negatives = num_negatives
        #self.list_all_pages = list_all_pages
        #self.alpha = constant* len(list_all_pages)
        #self.dic_cityid = dic_cityid
        #self.dic_cateid = dic_cateid
        #self.dic_districtid = dic_districtid
        #self.dic_projectid = dic_projectid
        #self.dic_streetid = dic_streetid
        #self.dic_wardid = dic_wardid
        #self.dic_priceid = dic_priceid
        #self.dic_areaid = dic_areaid

        #self.len_vector_item = {'dic_cityid': len(dic_cityid), 'dic_cateid': len(dic_cateid),
        #                        'dic_districtid': len(dic_districtid), 'dic_projectid': len(dic_projectid),
        #                        'dic_streetid': len(
        #                            dic_streetid), 'dic_wardid': len(dic_wardid), 'dic_priceid': len(dic_priceid),
        #                        'dic_areaid': len(dic_areaid)}
        #self.all_pages_id_attribute = get_all_pages_id_attribute(num_attribute)
        #self.list_count_att_all = count_occur_attribute_all_pages(self.list_all_pages,self.num_attribute)
        #self.list_item_vector = [[i] for i in range(num_attribute)]
        #self.list_item_vector_val = [[i] for i in range(num_attribute+1)]
        #self.type = type

    def create_batch_user_id(self, list_all_user_id):
        result = []
        for i in range(len(list_all_user_id)):
            user_id = int(list_all_user_id[i])
            list_user_id = [user_id] * (self.num_attribute + 1)
            result.append(list_user_id)
        return torch.LongTensor(np.array(result, dtype=np.long))

    def __len__(self):
        return self.nSamples

    def __getitem__(self, index):
        user_id = int(self.list_all_user_id[index])
        list_user_id = [user_id] * (self.num_attribute + 1)
        return (torch.LongTensor(np.array(list_user_id, dtype=np.long)))

    #def __getitem__(self, index):
    #    return self.batch_all_user_id[index]


def get_dataloaders(dic_map_userid_to_usi, num_attribute, batch_size):

    rc_dataset = rcoptimizeDatasets(dic_map_userid_to_usi,num_attribute)


    dataloaders = torch.utils.data.DataLoader(rc_dataset, batch_size,
                                                  shuffle=False, num_workers=4)
    return dataloaders