import numpy as np
import sys
sys.path.append("../")
from process_data.helper import *

def count_occur_attribute_all_pages(list_all_pages,num_attribute):
    result = [0 for i in range(num_attribute)]
    for item_vector in list_all_pages:
        for id in item_vector:
            if id < num_attribute:
                result[id]+=1
    return np.array(result)

if __name__ == '__main__':

    file_origin = '../process_data/file txt/user_view_train.txt'
    file_new = 'file txt/user_view_train.txt'

    dic_map_usi_to_userid = load_dic('../process_data/file npy/dic_map_usi_to_userid.npy')
    dic_item = load_dic('../process_data/file npy/dic_item.npy')
    dic_item_id_form = load_dic('../process_data/file npy/dic_item_id_form.npy')
    dic_cityid = load_dic('../process_data/file npy/dic_cityid.npy')
    dic_cateid = load_dic('../process_data/file npy/dic_cateid.npy')
    dic_districtid = load_dic('../process_data/file npy/dic_districtid.npy')
    dic_projectid = load_dic('../process_data/file npy/dic_projectid.npy')
    dic_streetid = load_dic('../process_data/file npy/dic_streetid.npy')
    dic_wardid = load_dic('../process_data/file npy/dic_wardid.npy')
    dic_priceid = load_dic('../process_data/file npy/dic_priceid.npy')
    dic_areaid = load_dic('../process_data/file npy/dic_areaid.npy')
    list_all_pages = np.load('../process_data/file npy/list_all_pages.npy')
    num_attribute = len(dic_cityid) + len(dic_cateid) + len(dic_districtid) + len(dic_projectid) + len(
        dic_streetid) + len(
        dic_wardid) + len(dic_priceid)+len(dic_areaid)

    list_count_att_all = count_occur_attribute_all_pages(list_all_pages,num_attribute)

    with open(file_origin,'r',encoding='utf-8') as fr, open(file_new,'w',encoding='utf-8') as fw:
        for line in fr:
            line = line.strip()
            line = line.split('****')
            user_id = int(line[0])
            line = line[1].split('||||')
            line = line[:len(line) - 1:]
            list_count_att_user = np.array([0 for i in range(num_attribute)])
            list_count_att_user_binary = np.array([0 for i in range(num_attribute)])
            n = len(list_all_pages) - len(line)
            m = 0
            for i in range(len(line)):
                page = line[i].split('::')
                r = int(page[0])
                m+=r
                index_page = int(page[1])
                item_vector = list_all_pages[index_page]
                for id in item_vector:
                    if id < num_attribute:
                        list_count_att_user[id]+=r
                        list_count_att_user_binary[id] += 1
            weight_list = list_count_att_user * n - m * (list_count_att_all - list_count_att_user_binary)
            print(float(n)/m)
            weight_list = weight_list.tolist()
            fw.write(str(user_id)+'****')
            for id in range(len(weight_list)):
                fw.write(str(id)+'::'+str(weight_list[id])+'||||')
            fw.write('\n')
            print('user '+str(user_id)+ ' ,num postive id: '+str(sum(x > 0 for x in weight_list))+ ' ,num negative id: '+str(sum(x < 0 for x in weight_list)))