import sys
sys.path.append("../")
import torch.nn as nn
import torch
import torch.nn.functional as F
import numpy as np
import math
'''
class Net(nn.Module):
    def __init__(self,num_user,factor_dim,num_attribute):
        super(Net, self).__init__()
        self.embedding_user = nn.Embedding(num_user,factor_dim)
        self.map_attribute_to_factor = nn.Linear(num_attribute,factor_dim,bias=False)

    def forward(self, u,I):
        #I = to_sparse(I)
        U = self.embedding_user(u)
        FI = self.map_attribute_to_factor(I)
        score = torch.bmm(U.view(U.size()[0], 1, U.size()[1]), FI.view(U.size()[0], U.size()[1], 1)).view(U.size()[0])
        score = F.sigmoid(score)
        return score
'''
class optimizeNet(nn.Module):
    def __init__(self,num_user,factor_dim,num_attribute):
        super(optimizeNet, self).__init__()
        self.embedding_user = nn.Embedding(num_user,factor_dim)
        range = 2*math.sqrt(0.25/factor_dim)
        embedding_user_initial = range/2.-range*np.random.rand(num_user,factor_dim)
        #range = math.sqrt(0.5/factor_dim)
        #embedding_user_initial = range * np.random.rand(num_user, factor_dim)
        self.embedding_user.weight.data.copy_(torch.from_numpy(embedding_user_initial))

        self.embedding_item = nn.Embedding(num_attribute+1,factor_dim,padding_idx=num_attribute)
        embedding_item_initial = range/2.-range*np.random.rand(num_attribute,factor_dim)
        #embedding_item_initial = -range * np.random.rand(num_attribute, factor_dim)
        embedding_item_initial = np.vstack((embedding_item_initial,np.zeros((1,factor_dim),dtype=np.float)))
        self.embedding_item.weight.data.copy_(torch.from_numpy(embedding_item_initial))
        #self.embedding_none = nn.Embedding(1,factor_dim).from_pretrained(torch.from_numpy()
        #self.map_attribute_to_factor = nn.Linear(num_attribute,factor_dim,bias=False)
        #self.num_attribute = num_attribute

    def forward(self, u,i):
        U = self.embedding_user(u)
        I = self.embedding_item(i)
        #print('embedding of None: ')
        #print(self.embedding_item(torch.LongTensor([6413]).cuda()))
        FI = torch.sum(I,dim = 1)
        score_ori = torch.bmm(U.view(U.size()[0], 1, U.size()[1]), FI.view(U.size()[0], U.size()[1], 1)).view(U.size()[0])
        #score = F.sigmoid(score_ori)
        #score = torch.sigmoid(score_ori)
        score = score_ori
        return score_ori, score


class Loss(nn.Module):
    def __init__(self,alpha, num_atrribue):
        super(Loss, self).__init__()
        self.alpha = alpha
        #self.p = torch.ones((num_atrribue,)).float()
    '''
    def forward(self, score, r): # mean square error loss
        p = (r > 0).float()
        c = (1 + self.alpha*r).float()
        loss = c * torch.pow(score - p,2)
        #loss = torch.mean(loss)
        #loss = torch.sum(loss) / torch.sum(c)
        loss = torch.sum(loss)
        return loss
    '''


    def forward(self, score, r): #cross entropy loss
        p = 2*(r > 0).float()-1
        #p = (r > 0).float()
        #p = torch.ones(r.size()).float().cuda()
        #loss = torch.abs(r).float() * F.binary_cross_entropy(score,p)
        loss = torch.abs(r).float() * (score - p)**2
        #loss = torch.sum(loss) / torch.sum(c)
        loss = torch.sum(loss)
        return loss



'''
def to_sparse(x):
    """ converts dense tensor x to sparse format """
    indices = torch.nonzero(x).t()
    values = x[tuple(indices[i] for i in range(indices.shape[0]))]
    return torch.sparse.FloatTensor(indices, values, x.size())
'''