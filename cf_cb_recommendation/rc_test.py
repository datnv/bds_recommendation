import sys

sys.path.append("../")
import torch
from cf_cb_recommendation.parameters import *
import cf_cb_recommendation.rc as rc
import cf_cb_recommendation.rc_input as rc_input
import torch.optim as optim
from torch.optim import lr_scheduler
import shutil
import os
import time
from torch import nn
from cf_cb_recommendation import rc_train

def get_ori_page(page,list_pair):
    ori_page = []
    based_index = 0
    for i in range(len(list_pair)):
        dic_name = list_pair[i][0]
        len_dic = list_pair[i][1]
        dic_inverse = list_pair[i][2]
        if (page[i]-based_index) in dic_inverse:
            ori_id = dic_inverse[page[i]-based_index]
            ori_page.append(str(ori_id))
        else:
            ori_page.append('None')
        based_index+=len_dic
    print(ori_page)
    return ori_page

def recommend_attribute_id(model, dataloaders, list_pair, dic_map_userid_to_usi):
    print('load parameters')
    checkpoint = torch.load('save/current/checkpoint.pth.tar')
    start_epoch = checkpoint['epoch']
    model.load_state_dict(checkpoint['state_dict'])

    start_time = time.time()

    phase = 'val'
    rank = 0
    weight_sum = 0
    for i, (batch_user_id, batch_item_vector, batch_r, batch_real_len) in enumerate(dataloaders[phase]):
        print(i)
        if i==80:
            break
        batch_size = batch_user_id.size()[0]
        num_page = batch_user_id.size()[1]
        batch_item_vector_ori = batch_item_vector.cpu().numpy()
        batch_user_id_ori = batch_user_id.cpu().numpy()
        batch_user_id, batch_item_vector, batch_r = rc_train.convert_data_to_normal_batch(batch_user_id,
                                                                                 batch_item_vector, batch_r)
        batch_user_id = batch_user_id.to(device)
        batch_item_vector = batch_item_vector.to(device)
        batch_r = batch_r.to(device)
        batch_real_len = batch_real_len.to(device)
        # print(batch_real_len.size())
        with torch.set_grad_enabled(phase == 'train'):
            score_ori, score = model(batch_user_id, batch_item_vector)
            score = score_ori.view(batch_size, num_page)
            #score_ori = score_ori.view(batch_size, num_page)
            batch_r = batch_r.view(batch_size, num_page)
            # print(score.size())
            for i in range(batch_size):
                list_item_vector = batch_item_vector_ori[i]
                usi = dic_map_userid_to_usi[batch_user_id_ori[i][0]]
                #print(usi)
                #get_ori_page(list_item_vector[0],list_pair)

                s = score[i]
                r = batch_r[i]
                s = s[:batch_real_len[i]:]
                r = r[:batch_real_len[i]:]
                s = s.cpu().numpy()
                r = r.cpu().numpy()
                #print(s)
                index_sort = sorted(range(len(s)), key=lambda k: s[k], reverse=True)
                '''
                get_ori_page(list_item_vector[index_sort[0]],list_pair)
                print(s[index_sort[0]])
                get_ori_page(list_item_vector[index_sort[1]],list_pair)
                print(s[index_sort[1]])
                get_ori_page(list_item_vector[index_sort[2]],list_pair)
                print(s[index_sort[2]])
                get_ori_page(list_item_vector[index_sort[3]],list_pair)
                print(s[index_sort[3]])
                print('---------------------')
                '''
                '''
                s = score_ori[i]
                r = batch_r[i]
                s = s[:batch_real_len[i]:]
                r = r[:batch_real_len[i]:]
                s = s.cpu().numpy()
                r = r.cpu().numpy()
                print(s)
                index_sort = sorted(range(len(s)), key=lambda k: s[k], reverse=True)
                get_ori_page(list_item_vector[index_sort[0]],list_pair)
                print(s[index_sort[0]])
                get_ori_page(list_item_vector[index_sort[1]],list_pair)
                print(s[index_sort[1]])
                get_ori_page(list_item_vector[index_sort[2]],list_pair)
                print(s[index_sort[2]])
                get_ori_page(list_item_vector[index_sort[3]],list_pair)
                print(s[index_sort[3]])
                print('=====================')
                '''

                for j in range(len(r)):
                    if r[j] == 0:
                        break
                    index = index_sort.index(j)
                    sub_rank = index / float(len(index_sort))
                    rank += r[j] * sub_rank
                    weight_sum += r[j]
                    # rank += sub_rank
                    # weight_sum+=1
    end_time = time.time()
    print('--------------------------')
    print('rank in test: ' + str(rank / float(weight_sum)))
    print('time: ' + str(end_time - start_time))
    print('--------------------------')


if __name__ == '__main__':
    dic_map_usi_to_userid = load_dic('../process_data/file npy/dic_map_usi_to_userid.npy')
    dic_map_userid_to_usi = load_dic('../process_data/file npy/dic_map_userid_to_usi.npy')
    dic_item = load_dic('../process_data/file npy/dic_item.npy')
    dic_item_id_form = load_dic('../process_data/file npy/dic_item_id_form.npy')
    dic_cityid = load_dic('../process_data/file npy/dic_cityid.npy')
    dic_cateid = load_dic('../process_data/file npy/dic_cateid.npy')
    dic_districtid = load_dic('../process_data/file npy/dic_districtid.npy')
    dic_projectid = load_dic('../process_data/file npy/dic_projectid.npy')
    dic_streetid = load_dic('../process_data/file npy/dic_streetid.npy')
    dic_wardid = load_dic('../process_data/file npy/dic_wardid.npy')
    dic_priceid = load_dic('../process_data/file npy/dic_priceid.npy')
    dic_areaid = load_dic('../process_data/file npy/dic_areaid.npy')

    dic_cityid_inverse = load_dic('../process_data/file npy/dic_cityid_inverse.npy')
    dic_cateid_inverse = load_dic('../process_data/file npy/dic_cateid_inverse.npy')
    dic_districtid_inverse = load_dic('../process_data/file npy/dic_districtid_inverse.npy')
    dic_projectid_inverse = load_dic('../process_data/file npy/dic_projectid_inverse.npy')
    dic_streetid_inverse = load_dic('../process_data/file npy/dic_streetid_inverse.npy')
    dic_wardid_inverse = load_dic('../process_data/file npy/dic_wardid_inverse.npy')
    dic_priceid_inverse = load_dic('../process_data/file npy/dic_priceid_inverse.npy')
    dic_areaid_inverse = load_dic('../process_data/file npy/dic_areaid_inverse.npy')

    list_all_pages = np.load('../process_data/file npy/list_all_pages.npy')
    num_attribute = len(dic_cityid) + len(dic_cateid) + len(dic_districtid) + len(dic_projectid) + len(
        dic_streetid) + len(
        dic_wardid) + len(dic_priceid) + len(dic_areaid)

    list_pair = [('dic_cityid', len(dic_cityid), dic_cityid_inverse),
                 ('dic_cateid', len(dic_cateid), dic_cateid_inverse),
                 ('dic_districtid', len(dic_districtid), dic_districtid_inverse),
                 ('dic_projectid', len(dic_projectid), dic_projectid_inverse),
                 ('dic_streetid', len(dic_streetid), dic_streetid_inverse),
                 ('dic_wardid', len(dic_wardid), dic_wardid_inverse),
                 ('dic_priceid', len(dic_priceid), dic_priceid_inverse),
                 ('dic_areaid', len(dic_areaid), dic_areaid_inverse)]

    use_cuda = use_cuda and torch.cuda.is_available()
    device = torch.device("cuda" if use_cuda else "cpu")

    model = rc.optimizeNet(len(dic_map_usi_to_userid.keys()), factor_dim, num_attribute)

    model = model.to(device)

    print(model)

    dataloaders = rc_input.get_dataloaders(dic_map_usi_to_userid, dic_item, dic_item_id_form, list_all_pages,
                                           dic_cityid, dic_cateid, dic_districtid,
                                           dic_projectid, dic_streetid, dic_wardid, dic_priceid, dic_areaid,
                                           num_attribute)

    recommend_attribute_id(model, dataloaders, list_pair, dic_map_userid_to_usi)
