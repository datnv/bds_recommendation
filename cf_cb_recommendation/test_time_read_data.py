import time

import sys
sys.path.append("../")
from torch.utils.data import Dataset
from random import randint
from process_data.helper import *
from cf_cb_recommendation.parameters import *
import torch
from random import shuffle
import random

"""
def convert_page_to_attribute_vector(page, dic_cityid, dic_cateid, dic_districtid, dic_projectid, dic_streetid,
                                     dic_wardid, len_vector_item):
    vector = []
    for i, pair in enumerate(
            [('dic_cityid', dic_cityid), ('dic_cateid', dic_cateid), ('dic_districtid', dic_districtid),
             ('dic_projectid', dic_projectid), ('dic_streetid', dic_streetid), ('dic_wardid', dic_wardid)]):
        # print(i)
        # print(pair)
        name_dic = pair[0]
        dic = pair[1]
        sub_vector = [0. for i in range(len_vector_item[name_dic])]
        if page[i] != 'None':
            if page[i].isdigit():
                page[i] = int(page[i])
            # print(page[i])
            # try:
            id = dic[page[i]]
            sub_vector[id] = 1.0
            '''
            except Exception as error:
                print('-----')
                print(error)
                print(page)
                print(name_dic)
                # print(len(sub_vector))
                # print(id)
                print(page[i])
            '''

        vector += sub_vector
    return vector
"""


def convert_page_to_list_indexes(page, dic_cityid, dic_cateid, dic_districtid, dic_projectid, dic_streetid,
                                 dic_wardid, dic_priceid, dic_areaid, len_vector_item, num_attribute):
    list_pair = [('dic_cityid', dic_cityid), ('dic_cateid', dic_cateid), ('dic_districtid', dic_districtid),
                 ('dic_projectid', dic_projectid), ('dic_streetid', dic_streetid), ('dic_wardid', dic_wardid),
                 ('dic_priceid', dic_priceid), ('dic_areaid', dic_areaid)]
    #vector = [str(num_attribute) for i in range(len(list_pair))]
    vector = [str(num_attribute)] * len(list_pair)
    base_index = 0
    for i, pair in enumerate(list_pair):
        # print(i)
        # print(pair)
        name_dic = pair[0]
        dic = pair[1]
        if page[i] != 'None':
            if page[i].isdigit():
                page[i] = int(page[i])
            # print(page[i])
            # try:
            id = dic[page[i]] + base_index
            vector[i] = str(id)
        base_index += len_vector_item[name_dic]
    return vector


'''

class rcDatasets(Dataset):
    def __init__(self, file, dic_map_usi_to_userid, dic_item, num_negatives, dic_cityid, dic_cateid, dic_districtid,
                 dic_projectid, dic_streetid, dic_wardid):
        with open(file, 'r') as file:
            self.lines = file.readlines()
        self.nSamples = len(self.lines)
        self.dic_map_usi_to_userid = dic_map_usi_to_userid
        self.dic_item = dic_item
        self.num_negatives = num_negatives
        self.list_all_pages = list(self.dic_item.keys())
        self.dic_cityid = dic_cityid
        self.dic_cateid = dic_cateid
        self.dic_districtid = dic_districtid
        self.dic_projectid = dic_projectid
        self.dic_streetid = dic_streetid
        self.dic_wardid = dic_wardid
        self.len_vector_item = {'dic_cityid': len(dic_cityid), 'dic_cateid': len(dic_cateid),
                                'dic_districtid': len(dic_districtid), 'dic_projectid': len(dic_projectid),
                                'dic_streetid': len(
                                    dic_streetid), 'dic_wardid': len(dic_wardid)}

    def __len__(self):
        # return self.nSamples
        return 20

    def __getitem__(self, index):
        list_user_id = []
        list_item_vector = []
        list_r = []
        line = self.lines[index].rstrip()
        line = line.split('****')
        user_id = self.dic_map_usi_to_userid[line[0]]
        line = line[1].split('||||')
        line = line[:len(line) - 1:]
        # print(line)
        dic_view_page = {}
        for i in range(len(line)):
            page = line[i].split('::')
            # print(page)
            r = int(page[0])
            page = page[1::]
            dic_view_page['::'.join(page)] = 1
            item_vector = convert_page_to_attribute_vector(page, self.dic_cityid, self.dic_cateid, self.dic_districtid,
                                                           self.dic_projectid, self.dic_streetid,
                                                           self.dic_wardid, self.len_vector_item)
            list_user_id.append(user_id)
            list_item_vector.append(item_vector)
            list_r.append(r)

        for i in range(self.num_negatives):
            page_id = randint(0, len(self.list_all_pages) - 1)
            page = self.list_all_pages[page_id]
            if page not in dic_view_page.keys():
                page = page.split('::')
                r = 0
                item_vector = convert_page_to_attribute_vector(page, self.dic_cityid, self.dic_cateid,
                                                               self.dic_districtid, self.dic_projectid,
                                                               self.dic_streetid,
                                                               self.dic_wardid, self.len_vector_item)
                list_user_id.append(user_id)
                list_item_vector.append(item_vector)
                list_r.append(r)
        # print(np.shape(np.array(list_user_id, dtype=np.int32)))
        # print(np.shape(np.array(list_item_vector, dtype=np.float32)))
        # print(np.shape(np.array(list_r, dtype=np.int32)))
        return (torch.LongTensor(np.array(list_user_id, dtype=np.long)),
                torch.from_numpy(np.array(list_item_vector, dtype=np.float32)),
                torch.from_numpy(np.array(list_r, dtype=np.int32)))
'''


class rcoptimizeDatasets(Dataset):
    def __init__(self, file, dic_map_usi_to_userid, dic_item, dic_item_id_form, list_all_pages, num_negatives, num_page_per_user, dic_cityid, dic_cateid,
                 dic_districtid,
                 dic_projectid, dic_streetid, dic_wardid, dic_priceid, dic_areaid, num_attribute, type):
        with open(file, 'r') as file:
            self.lines = file.readlines()
        self.nSamples = len(self.lines)
        #self.dic_map_usi_to_userid = dic_map_usi_to_userid
        #self.dic_item = dic_item
        #self.num_negatives = num_negatives
        self.num_page_per_user = num_page_per_user
        self.list_all_pages = list_all_pages
        #self.dic_cityid = dic_cityid
        #self.dic_cateid = dic_cateid
        #self.dic_districtid = dic_districtid
        #self.dic_projectid = dic_projectid
        #self.dic_streetid = dic_streetid
        #self.dic_wardid = dic_wardid
        #self.dic_priceid = dic_priceid
        #self.dic_areaid = dic_areaid
        #self.num_attribute = num_attribute
        #self.len_vector_item = {'dic_cityid': len(dic_cityid), 'dic_cateid': len(dic_cateid),
        #                        'dic_districtid': len(dic_districtid), 'dic_projectid': len(dic_projectid),
        #                        'dic_streetid': len(
        #                            dic_streetid), 'dic_wardid': len(dic_wardid), 'dic_priceid': len(dic_priceid),
        #                        'dic_areaid': len(dic_areaid)}
        self.type = type

    def __len__(self):
        return self.nSamples
        #return 50

    def __getitem__(self, index):
        list_user_id = []
        list_item_vector = []
        list_r = []
        if self.type == 'train':
            line = self.lines[index].rstrip()
            line = line.split('****')
            user_id = int(line[0])
            line = line[1].split('||||')
            line = line[:len(line) - 1:]
            # print(line)
            dic_view_page = {}
            list1 = list(range(len(line)))
            shuffle(list1)
            for i in list1:
                page = line[i].split('::')
                # print(page)
                r = int(page[0])
                index_page = int(page[1])
                item_vector = self.list_all_pages[index_page]
                dic_view_page[index_page] = 1
                #item_vector = [int(page[i]) for i in range(len(page))]
                list_user_id.append(user_id)
                list_item_vector.append(item_vector)
                list_r.append(r)

            # for i in range(self.num_page_per_user - len(line)):
            list_index_page_add = random.sample(range(0,len(self.list_all_pages)),self.num_page_per_user-len(dic_view_page.keys()))
            list_user_id.extend([user_id] * len(list_index_page_add))
            list_item_vector.extend([self.list_all_pages[i] for i in list_index_page_add])
            list_r.extend([0] * len(list_index_page_add))
            '''
            while len(list_user_id) <= self.num_page_per_user:
                page_id = randint(0, len(self.list_all_pages) - 1)
                if page_id in dic_view_page.keys():
                    continue
                item_vector = self.list_all_pages[page_id]
                r = 0
                #item_vector = [int(page[i]) for i in range(len(page))]
                list_user_id.append(user_id)
                list_item_vector.append(item_vector)
                list_r.append(r)
            '''
            # print(np.shape(np.array(list_user_id, dtype=np.int32)))
            # print(np.shape(np.array(list_item_vector, dtype=np.float32)))
            # print(np.shape(np.array(list_r, dtype=np.int32)))
            list_user_id = list_user_id[:self.num_page_per_user:]
            list_item_vector = list_item_vector[:self.num_page_per_user:]
            list_r = list_r[:self.num_page_per_user:]
            return (torch.LongTensor(np.array(list_user_id, dtype=np.long)),
                    torch.LongTensor(np.array(list_item_vector, dtype=np.long)),
                    torch.IntTensor(np.array(list_r, dtype=np.int32)))
        elif self.type == 'val':
            line = self.lines[index].rstrip()
            line = line.split('****')
            user_id = int(line[0])
            line_val = line[1].split('||||')
            line_val = line_val[:len(line_val) - 1:]
            line_train = line[2].split('||||')
            line_train = line_train[:len(line_train) - 1:]
            dic_view_page_train = {}
            dic_view_page_val = {}
            for i in range(len(line_val)):
                page = line_val[i].split('::')
                r = int(page[0])
                index_page = int(page[1])
                dic_view_page_val[index_page] = 1
                item_vector = self.list_all_pages[index_page]
                #item_vector = [int(page[i]) for i in range(len(page))]
                list_user_id.append(user_id)
                list_item_vector.append(item_vector)
                list_r.append(r)
            for i in range(len(line_train)):
                page = line_train[i].split('::')
                index_page = int(page[1])
                dic_view_page_train[index_page] = 1
            for index_page in range(len(self.list_all_pages)):
                if index_page not in dic_view_page_val and index_page not in dic_view_page_train:
                    r = 0
                    item_vector = self.list_all_pages[index_page]
                    #item_vector = [int(page[i]) for i in range(len(page))]
                    list_user_id.append(user_id)
                    list_item_vector.append(item_vector)
                    list_r.append(r)
            real_len = [len(list_user_id)]
            while len(list_user_id) < len(self.list_all_pages):
                r = 0
                list_user_id.append(user_id)
                #list_item_vector.append([self.num_attribute for i in range(len(self.len_vector_item.keys()))])
                list_item_vector.append([self.num_attribute]*len(self.len_vector_item.keys()))
                list_r.append(r)
            # print(len(list_user_id))
            # print(len(list_item_vector))
            # print(len(list_r))
            # print(real_len)
            return (torch.LongTensor(np.array(list_user_id, dtype=np.long)),
                    torch.LongTensor(np.array(list_item_vector, dtype=np.long)),
                    torch.IntTensor(np.array(list_r, dtype=np.int32)),
                    torch.IntTensor(np.array(real_len)))


def get_dataloaders(dic_map_usi_to_userid, dic_item,dic_item_id_form,list_all_pages, dic_cityid, dic_cateid, dic_districtid, dic_projectid,
                    dic_streetid, dic_wardid, dic_priceid, dic_areaid, num_attribute):
    rc_dataset = {
        x: rcoptimizeDatasets('../process_data/file txt/user_view_' + x + '.txt', dic_map_usi_to_userid, dic_item,
                              dic_item_id_form,list_all_pages, num_negatives, num_page_per_user,
                              dic_cityid, dic_cateid, dic_districtid,
                              dic_projectid, dic_streetid, dic_wardid, dic_priceid, dic_areaid, num_attribute, x) for x
        in ['train', 'val']}

    '''
    rc_dataset = {
        x: rcDatasets('../process_data/file txt/user_view_' + x + '.txt', dic_map_usi_to_userid, dic_item,
                      num_negatives,
                      dic_cityid, dic_cateid, dic_districtid,
                      dic_projectid, dic_streetid, dic_wardid) for x in ['train', 'val']}
    '''

    dataloaders = {x: torch.utils.data.DataLoader(rc_dataset[x], batch_size[x],
                                                  shuffle=True, num_workers=1)
                   for x in ['train', 'val']}
    return dataloaders

if __name__ == '__main__':
    dic_map_usi_to_userid = load_dic('../process_data/file npy/dic_map_usi_to_userid.npy')
    dic_item = load_dic('../process_data/file npy/dic_item.npy')
    dic_item_id_form = load_dic('../process_data/file npy/dic_item_id_form.npy')
    dic_cityid = load_dic('../process_data/file npy/dic_cityid.npy')
    dic_cateid = load_dic('../process_data/file npy/dic_cateid.npy')
    dic_districtid = load_dic('../process_data/file npy/dic_districtid.npy')
    dic_projectid = load_dic('../process_data/file npy/dic_projectid.npy')
    dic_streetid = load_dic('../process_data/file npy/dic_streetid.npy')
    dic_wardid = load_dic('../process_data/file npy/dic_wardid.npy')
    dic_priceid = load_dic('../process_data/file npy/dic_priceid.npy')
    dic_areaid = load_dic('../process_data/file npy/dic_areaid.npy')
    list_all_pages = np.load('../process_data/file npy/list_all_pages.npy')
    num_attribute = len(dic_cityid) + len(dic_cateid) + len(dic_districtid) + len(dic_projectid) + len(
        dic_streetid) + len(
        dic_wardid) + len(dic_priceid)+len(dic_areaid)

    dataloaders = get_dataloaders(dic_map_usi_to_userid, dic_item,dic_item_id_form, list_all_pages, dic_cityid, dic_cateid, dic_districtid,
                                           dic_projectid, dic_streetid, dic_wardid, dic_priceid, dic_areaid, num_attribute)
    start_time = time.time()
    end_time = 0
    for i, (batch_user_id, batch_item_vector, batch_r) in enumerate(dataloaders['train']):
        end_time = time.time()
        print('time to load a batch data: '+str(end_time-start_time))
        start_time = time.time()

