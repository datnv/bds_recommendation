import sys
sys.path.append("../")
import torch
from cf_cb_recommendation.parameters import *
import cf_cb_recommendation.rc as rc
import cf_cb_recommendation.rc_input as rc_input
import torch.optim as optim
from torch.optim import lr_scheduler
import shutil
import os
import time
from torch import nn



def save_checkpoint(state, is_max, filename='save/current/checkpoint.pth.tar'):
    torch.save(state, filename)
    torch.save(state, 'save/reserve/checkpoint.pth.tar')
    if is_max:
        shutil.copyfile(filename, 'save/max/model_best.pth.tar')


def set_lnr(optimizer, new_lnr):
    for g in optimizer.param_groups:
        # optim.param_groups is a list of the different weight groups which can have different learning rates
        g['lr'] = new_lnr

def convert_data_to_normal_batch(batch_user_id, batch_item_vector, batch_r):
    new_batch_user_id = batch_user_id.view(batch_user_id.size()[0]*batch_user_id.size()[1])
    new_batch_item_vector = batch_item_vector.view(batch_item_vector.size()[0]*batch_item_vector.size()[1],batch_item_vector.size()[2])
    new_batch_r = batch_r.view(batch_r.size()[0]*batch_r.size()[1])
    return new_batch_user_id, new_batch_item_vector, new_batch_r
'''
def convert_data_to_normal_batch(batch_user_id, batch_item_vector, batch_r):
    new_batch_user_id = batch_user_id.view(batch_user_id.size()[1])
    new_batch_item_vector = batch_item_vector.view(batch_item_vector.size()[1], batch_item_vector.size()[2])
    new_batch_r = batch_r.view(batch_r.size()[1])
    return new_batch_user_id, new_batch_item_vector, new_batch_r
'''

def train_model(model, criterion, optimizer, scheduler, dataloaders, num_epochs):
    if os.path.isfile('save/current/checkpoint.pth.tar'):
        print('load parameters')
        checkpoint = torch.load('save/current/checkpoint.pth.tar')
        start_epoch = checkpoint['epoch']
        model.load_state_dict(checkpoint['state_dict'])
        optimizer.load_state_dict(checkpoint['optimizer'])
    else:
        print('not load parameters')
        start_epoch = 1

    print('start epoch: ' + str(start_epoch))

    #set_lnr(optimizer,1e-3)
    print('learning rate: ' + str(optimizer.param_groups[0]['lr']))

    for epoch in range(start_epoch, num_epochs):
        if epoch == 40:
            current_lnr = optimizer.param_groups[0]['lr']
            set_lnr(optimizer, current_lnr/10.)
        if epoch == 80:
            current_lnr = optimizer.param_groups[0]['lr']
            set_lnr(optimizer, current_lnr/10.)
        list_phase = []
        if epoch % 10 == -1:
            list_phase = ['train', 'val']
        else:
            list_phase = ['train']

        for phase in list_phase:
            start_time = time.time()
            if phase == 'train':
                # scheduler.step()
                model.train()  # Set model to training mode
            else:
                model.eval()  # Set model to evaluate mode

            if phase == 'train':
                running_loss = 0.0
                dataset_sizes = 0
                # Iterate over data.
                for i, (batch_user_id, batch_item_vector, batch_r) in enumerate(dataloaders[phase]):
                    #print(i)
                    # print('type(batch_user_id): '+str(type(batch_user_id)))
                    batch_user_id, batch_item_vector, batch_r = convert_data_to_normal_batch(batch_user_id,
                                                                                             batch_item_vector, batch_r)
                    # print(np.shape(np.array(batch_user_id)))
                    # print(batch_item_vector)
                    # print(np.shape(np.array(batch_item_vector)))
                    # print(np.shape(np.array(batch_r)))
                    batch_user_id = batch_user_id.to(device)
                    batch_item_vector = batch_item_vector.to(device)
                    batch_r = batch_r.to(device)
                    # zero the parameter gradients
                    optimizer.zero_grad()

                    # forward
                    # track history if only in train
                    with torch.set_grad_enabled(phase == 'train'):
                        # if in val phase set many crops

                        score_ori, score = model(batch_user_id, batch_item_vector)

                        loss = criterion(score, batch_r)

                        # backward + optimize only if in training phase
                        if phase == 'train':
                            loss.backward()
                            optimizer.step()

                    # statistics
                    running_loss += loss.item()
                    dataset_sizes += batch_user_id.size(0)

                epoch_loss = running_loss / dataset_sizes

                end_time = time.time()

                print('{} Loss: {:.4f} Lnr: {} Time: {}'.format(
                    phase, epoch_loss, str(optimizer.param_groups[0]['lr']), str(end_time - start_time)))

            elif phase == 'val':
                rank = 0
                weight_sum = 0
                for i, (batch_user_id, batch_item_vector, batch_r, batch_real_len) in enumerate(dataloaders[phase]):
                    print(i)
                    batch_size = batch_user_id.size()[0]
                    num_page = batch_user_id.size()[1]
                    batch_user_id, batch_item_vector, batch_r = convert_data_to_normal_batch(batch_user_id,
                                                                                             batch_item_vector, batch_r)
                    batch_user_id = batch_user_id.to(device)
                    batch_item_vector = batch_item_vector.to(device)
                    batch_r = batch_r.to(device)
                    batch_real_len = batch_real_len.to(device)
                    #print(batch_real_len.size())
                    with torch.set_grad_enabled(phase == 'train'):
                        score_ori,score = model(batch_user_id, batch_item_vector)
                        score = score.view(batch_size,num_page)
                        batch_r = batch_r.view(batch_size,num_page)
                        #print(score.size())
                        for i in range(batch_size):
                            s = score[i]
                            r = batch_r[i]
                            s = s[:batch_real_len[i]:]
                            r = r[:batch_real_len[i]:]
                            s = s.numpy()
                            r = r.numpy()
                            index_sort = sorted(range(len(s)), key=lambda k: s[k],reverse=True)
                            for j in range(len(r)):
                                if r[j]==0:
                                    break
                                index = index_sort.index(j)
                                sub_rank = index / float(len(index_sort))
                                rank+=r[j]*sub_rank
                                weight_sum+=r[j]
                                #rank += sub_rank
                                #weight_sum+=1
                end_time = time.time()
                print('--------------------------')
                print('rank in test: '+str(rank/float(weight_sum)))
                print('time: '+str(end_time - start_time))
                print('--------------------------')



        if epoch % 1 == 0:
            save_checkpoint({
                'epoch': epoch + 1,
                'state_dict': model.state_dict(),
                'optimizer': optimizer.state_dict(),
            }, False)

        #break

if __name__ == '__main__':
    dic_map_usi_to_userid = load_dic('../process_data/file npy/dic_map_usi_to_userid.npy')
    dic_item = load_dic('../process_data/file npy/dic_item.npy')
    dic_item_id_form = load_dic('../process_data/file npy/dic_item_id_form.npy')
    dic_cityid = load_dic('../process_data/file npy/dic_cityid.npy')
    dic_cateid = load_dic('../process_data/file npy/dic_cateid.npy')
    dic_districtid = load_dic('../process_data/file npy/dic_districtid.npy')
    dic_projectid = load_dic('../process_data/file npy/dic_projectid.npy')
    dic_streetid = load_dic('../process_data/file npy/dic_streetid.npy')
    dic_wardid = load_dic('../process_data/file npy/dic_wardid.npy')
    dic_priceid = load_dic('../process_data/file npy/dic_priceid.npy')
    dic_areaid = load_dic('../process_data/file npy/dic_areaid.npy')
    list_all_pages = np.load('../process_data/file npy/list_all_pages.npy')
    num_attribute = len(dic_cityid) + len(dic_cateid) + len(dic_districtid) + len(dic_projectid) + len(
        dic_streetid) + len(
        dic_wardid) + len(dic_priceid)+len(dic_areaid)

    use_cuda = use_cuda and torch.cuda.is_available()
    device = torch.device("cuda" if use_cuda else "cpu")

    #model = rc.Net(len(dic_map_usi_to_userid.keys()), factor_dim, num_attribute)
    model = rc.optimizeNet(len(dic_map_usi_to_userid.keys()), factor_dim, num_attribute)
    '''
    if torch.cuda.device_count() > 1:
        print("Let's use", torch.cuda.device_count(), "GPUs!")
        # dim = 0 [30, xxx] -> [10, ...], [10, ...], [10, ...] on 3 GPUs
        model = nn.DataParallel(model)
    '''
    model = model.to(device)
    print(model)

    dataloaders = rc_input.get_dataloaders(dic_map_usi_to_userid, dic_item,dic_item_id_form, list_all_pages, dic_cityid, dic_cateid, dic_districtid,
                                           dic_projectid, dic_streetid, dic_wardid, dic_priceid, dic_areaid, num_attribute)

    criterion = rc.Loss(alpha)
    criterion = criterion.to(device)

    params_to_update = model.parameters()

    optimizer = optim.SGD(params_to_update, lr=initial_lnr, momentum=0.9, weight_decay=0.0001)

    exp_lr_scheduler = lr_scheduler.StepLR(optimizer, step_size=7, gamma=0.1)

    train_model(model, criterion, optimizer, exp_lr_scheduler, dataloaders, num_epochs=120)
